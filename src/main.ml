
open Owlapi

let load_ontology s_iri =
  let ont_manager : OntologyManager.obj = Manager.create_OntologyManager () in
  let iri : IRI.obj = IRI.create_from_string s_iri in
  let ontology : Ontology.obj = ont_manager#load_ontology_from_iri iri in
  print_endline ("Loaded: " ^ ontology#to_string);
  ontology

let reasoning () =
  let ont = load_ontology "http://owl.cs.manchester.ac.uk/repository/download?ontology=file:/Users/seanb/Desktop/Cercedilla2005/hands-on/people.owl&format=RDF/XML" in
  let reasoner_factory = ReasonerFactory.create () in
  let reasoner = reasoner_factory#create_reasoner ont in
  if reasoner#is_consistent
  then print_endline "This is consistent!"
  else print_endline "Oops! This is not consistent."

let _ =
  debug (fun () ->
(*    ignore (load_ontology "http://www.co-ode.org/ontologies/pizza/pizza.owl"); *)
    reasoning ())
