
open Owlapi

type iri = string

type cls =
  | Thing
  | Class of iri
  | ObjectExists of prop * cls
  | Individual of iri
  | Compl of cls
  | Inter of cls list
  | Union of cls list
and prop =
  | ObjectProperty of iri
  | Inverse of prop

type axiom =
| Rename of iri * iri
| Possible of cls
| Necessary of cls * cls
| All_necessary of cls * cls list
| Impossible of cls
| All_impossible of cls * cls list
| Definition of iri * cls
| Instance of iri * cls
| Anonymous_instance of cls
| New_class of iri
| New_property of iri
| New_inverse_property of iri
| Subclassof of iri * iri
| Subpropertyof of iri * iri
| Property_inverse of iri * iri
| Functional of prop
| Inverse_functional of prop
| Reflexive of prop
| Irreflexive of prop
| Symmetric of prop
| Asymmetric of prop
| Transitive of prop
| All_disjoint of cls list


let rec compare c1 c2 = 
  match c1, c2 with
  | Compl d1, Compl d2 -> compare d1 d2
  | _, Compl d2 -> if c1 = d2 then -1 else compare c1 d2
  | Compl d1, _ -> if d1 = c2 then 1 else compare d1 c2
  | ObjectExists (p1, c1), ObjectExists (p2,c2) ->
    let cmp = compare c1 c2 in
    if cmp <> 0 then cmp else Pervasives.compare p1 p2
  | _, _ -> Pervasives.compare c1 c2

(*
let simpl_inter_list = function
  | [] -> Thing
  | [x] -> x
  | l -> Inter l
*)

let simpl_inter c1 c2 =
  match c1, c2 with
  | Thing, _ -> c2
  | _, Thing -> c1
  | Inter lc1, Inter lc2 -> Inter (lc1 @ lc2)
  | _, Inter lc2 -> Inter (c1::lc2)
  | Inter lc1, _ -> Inter (lc1 @ [c2])
  | _, _ -> Inter [c1; c2]


let simpl_union = function
  | [] -> invalid_arg "Expression.simpl_union_list"
  | [x] -> x
  | l -> Union l

let rec owlapi_of_cls (data_fact : DataFactory.obj) : cls -> ClassExpression.obj = function
  | Individual iri ->
      data_fact#get_object_one_of [(data_fact#get_named_individual (IRI.create iri) :> Individual.obj)]
  | Class iri ->
      (data_fact#get_class (IRI.create iri) :> ClassExpression.obj)
  | ObjectExists (p, c1) ->
      data_fact#get_object_some_values_from (owlapi_of_prop data_fact p) (owlapi_of_cls data_fact c1)
  | Thing ->
      (data_fact#get_thing :> ClassExpression.obj)
  | Compl c1 ->
      data_fact#get_object_complement_of (owlapi_of_cls data_fact c1)
  | Inter lc1 ->
      data_fact#get_object_intersection_of (List.map (owlapi_of_cls data_fact) lc1)
  | Union lc1 ->
      data_fact#get_object_union_of (List.map (owlapi_of_cls data_fact) lc1)
and owlapi_of_prop data_fact : prop -> ObjectPropertyExpression.obj = function
  | ObjectProperty iri ->
      (data_fact#get_object_property (IRI.create iri) :> ObjectPropertyExpression.obj)
  | Inverse p1 ->
      data_fact#get_object_inverse_of (owlapi_of_prop data_fact p1)


type focus = int list
type focus_cls = focus * cls

type context = (int * cls) list
type cls_context = cls * context

let rec get_context ?acc:(k : context = []) (ps, c : focus_cls) : cls_context =
  match ps, c with
  | [], _ -> c, k
  | 0::ps1, ObjectExists (p, c1) ->
      get_context (ps1,c1) ~acc:((0,c)::k)
(*
  | 0::ps1, Compl c1 ->
      get_context (ps1, c1) ~acc:((0,c)::k)
*)
  | n::ps1, Inter lc when 0 <= n && n < List.length lc ->
      get_context (ps1, List.nth lc n) ~acc:((n,c)::k)
  | n::ps1, Union lc when 0 <= n && n < List.length lc ->
      get_context (ps1, List.nth lc n) ~acc:((n,c)::k)
  | _ -> invalid_arg "Expression.get_context: invalid_path"

let rec get_focus ?acc:(ps : focus = []) (x, k : cls_context) : focus_cls =
  match k with
  | [] -> ps, x
  | (n,c)::k' ->
      match n, c with
      | 0, ObjectExists (p, c1) ->
	  let c' = if x = c1 then c else ObjectExists (p, x) in
	  get_focus ~acc:(0::ps) (c',k')
(*
      | 0, Compl c1 ->
	  let c' = if x = c1 then c else Compl x in
	  get_focus ~acc:(0::ps) (c',k')
*)
      | n, Inter lc ->
	  let c' = if x = List.nth lc n then c else Inter (Common.list_set_nth lc n x) in
	  get_focus ~acc:(n::ps) (c',k')
      | n, Union lc ->
	  let c' = if x = List.nth lc n then c else Union (Common.list_set_nth lc n x) in
	  get_focus ~acc:(n::ps) (c',k')
      | _ -> assert false

let rec simpl_context_thing (k : context) : cls_context =
  match k with
  | [] -> Thing, []
  | (n,c)::k' ->
      match n, c with
      | n, Inter lc1 ->
	  ( match Common.list_remove_nth lc1 n with
	  | [] -> simpl_context_thing k'
	  | [x] -> x, k'
	  | lc1' -> Inter lc1', k')
      | n, Union lc1 ->
	  ( match Common.list_remove_nth lc1 n with
	  | [] -> simpl_context_thing k'
	  | [x] -> x, k'
	  | lc1' -> Union lc1', k')
      | _ ->
	  Thing, k

let rec simpl_context_assoc (k : context) : context =
  match k with
  | (n1, Inter l1)::(n2, Inter l2)::k' ->
      simpl_context_assoc ((n2+n1, Inter (Common.list_insert_nth l2 n2 l1))::k')
  | (n1, Union l1)::(n2, Union l2)::k' ->
      simpl_context_assoc ((n2+n1, Union (Common.list_insert_nth l2 n2 l1))::k')
  | (n1,f1)::k' ->
      (n1,f1)::simpl_context_assoc k'
  | [] -> []

let rec fold_focus (ff : 'a -> bool (* prefix *) -> focus -> context -> cls -> 'a) (res : 'a) (ps : focus) (k : context) (c : cls) : 'a =
  let res = ff res true ps k c in
  let res =
    match c with
    | Individual _ -> res
    | Class _ -> res
    | ObjectExists (p, c1) ->
	fold_focus ff res (ps@[0]) ((0,c)::k) c1
    | Thing -> res
    | Compl c1 -> res
    | Inter lc ->
	let _, res =
	  List.fold_left
	    (fun (i,res) ci -> i+1, fold_focus ff res (ps@[i]) ((i,c)::k) ci)
	    (0,res) lc in
	res
    | Union lc ->
	let _, res =
	  List.fold_left
	    (fun (i,res) ci -> i+1, fold_focus ff res (ps@[i]) ((i,c)::k) ci)
	    (0,res) lc in
	res in
  let res = ff res false ps k c in
  res

let focus_next_prefix (path,q) =
  let first_opt, next_opt =
    fold_focus
      (fun (first_opt, next_opt) prefix ps k f ->
	match f, k with
	| Individual _, _
	| _, (_, Inter _)::_ ->
	    first_opt,
	    next_opt
	| _ ->
	    (if first_opt = None then Some ps else first_opt),
	    (if next_opt = None && ps > path then Some ps else next_opt))
      (None, None) [] [] q in
  match next_opt with
  | Some ps -> (ps,q)
  | None ->
      match first_opt with
      | Some ps -> (ps,q)
      | None -> ([],q)

let focus_next_postfix (path,q) =
  let _, next_opt =
    fold_focus
      (fun (after_path, next_opt) prefix ps k f ->
	(after_path || ps = path),
	( match f, k with
	| Individual _, _
	| _, (_, Inter _)::_ ->
	    next_opt
	| _ ->
	    (if next_opt = None && after_path && ps <> path then Some ps else next_opt)))
      (false,None) [] [] q in
  match next_opt with
  | Some ps -> (ps,q)
  | None -> (path,q)


let rec reverse_context ?(acc : context = []) = function
  | [] -> acc
  | (n,c)::k' ->
      match n, c with
      | 0, ObjectExists (p, c1) ->
	  let acc' = (0, ObjectExists (reverse_property p, Thing))::acc in
	  reverse_context ~acc:acc' k'
(*
      | 0, Compl _ ->
	  reverse_context ~acc:acc' k'
*)
      | n, Inter lc ->
	  let acc' = (n, Inter lc)::acc in
	  reverse_context ~acc:acc' k'
      | n, Union lc ->
	  let acc' = acc in
	  reverse_context ~acc:acc' k'
      | _ -> assert false
and reverse_property p =
  match p with
  | ObjectProperty _ -> Inverse p
  | Inverse p1 -> p1

let get_reverse_context (ps_c : focus_cls) : cls * cls =
  let c1, k = get_context ps_c in
  let rev_k = reverse_context k in
  let _, rev_c = get_focus (simpl_context_thing rev_k) in
  c1, rev_c

let focus_reversal (ps_c : focus_cls) : cls =
  let c1, ck = get_reverse_context ps_c in
  simpl_inter c1 ck

let focus_navlink (transf : cls_context -> focus * cls * context) (ps_c : focus_cls) =
  let c1, k = get_context ps_c in
  let ps1, c1', k' = transf (c1,k) in
  get_focus ~acc:ps1 (c1',k')


let focus_zoom store ps_c x =
  let rec entails store c1 c2 =
    match c1, c2 with
    | _, Thing -> true
    | Individual _, Individual _
    | Individual _, Compl (Individual _)
    | Compl (Individual _), Individual _
    | Class _, Class _ ->
	let ce1 = owlapi_of_cls store#data_factory c1 in
	let ce2 = owlapi_of_cls store#data_factory c2 in
	store#reasoner#is_entailed (store#data_factory#get_subclass_of ce1 ce2)
    | _ -> false
  in
  focus_navlink
    (fun (c1,k) ->
      let lc1, k =
	match k with
	| (n, Inter lc1)::k' -> lc1, k'
	| _ -> [c1], k in
      let add, rev_l =
	List.fold_left
	  (fun (add, rev_l) c ->
	    let c_x = entails store c x in
	    let x_c = entails store x c in
	    add && not (c_x && x_c),
	    if c_x || x_c
	    then rev_l
	    else c::rev_l)
	  (true, []) lc1 in
      let rev_l = if add then x::rev_l else rev_l in
      match rev_l with
      | [] -> [], Thing, k
      | [c1'] -> [], c1', k
      | lc1' -> [List.length lc1' - 1], Inter (List.rev lc1'), k)
    ps_c

let focus_union ps_c =
  focus_navlink
    (fun (c1,k) ->
      let c1, k = (* setting focus on above disjunction, if any *)
	match k with
	| (n, Union l)::k' -> Union l, k'
	| _ -> c1, k in
      match c1 with
      | Union lc -> [List.length lc], Union (lc @ [Thing]), k
      | _ -> [1], Union [c1; Thing], k)
    ps_c

let focus_compl ps_c =
  focus_navlink
    (fun (c1,k) ->
      match c1 with
      | Compl c2 -> [], c2, k
      | _ -> [], Compl c1, k)
    ps_c

let focus_select ps_c : focus_cls =
  let c1, k = get_context ps_c in
  [], c1

let focus_delete ps_c =
  focus_navlink
    (fun (_,k) ->
      let c1, k' = simpl_context_thing k in
      [], c1, k')
    ps_c

type prec = int
let prec_or = 4
let prec_and = 3
let prec_not = 2
let prec_restr = 1
let prec_atom = 0

type print_tag = Tag_modifier | Tag_individual | Tag_class | Tag_property | Tag_path of focus

let print_in_scope ctx_prec op_prec p pp =
  if op_prec > ctx_prec
  then begin pp#open_scope; p pp; pp#close_scope end
  else begin pp#break; p pp end


class virtual print pp =
  let extends_ps ps_opt n =
    Option.map (fun ps -> ps@[n]) ps_opt in
  object (self)
    method apply_axiom = function
    | Rename (iri,new_iri) -> self#print_iri_bare iri; pp#string " is renamed as "; self#print_iri_bare new_iri
    | Possible c -> self#apply_cls c; pp#string " is possible"
    | Necessary (c1,c2) -> self#apply_cls c2; pp#string " is necessary for "; self#apply_cls c1
    | Impossible c -> self#apply_cls c; pp#string " is impossible"
    | All_necessary (_,[]) -> assert false
    | All_necessary (c1,[c2]) -> self#apply_axiom (Necessary (c1,c2))
    | All_necessary (c1,c2::lc2) ->
      pp#string "all of "; self#apply_cls c2;
      List.iter (fun c2 -> pp#string ", "; self#apply_cls c2) lc2;
      pp#string " are necessary for ";
      self#apply_cls c1
    | All_impossible (_,[]) -> assert false
    | All_impossible (c1,[c2]) ->
      self#apply_cls c2;
      pp#string " is impossible for ";
      self#apply_cls c1
    | All_impossible (c1,c2::lc2) ->
      pp#string "all of "; self#apply_cls c2;
      List.iter (fun c2 -> pp#string ", "; self#apply_cls c2) lc2;
      pp#string " are impossible for ";
      self#apply_cls c1
    | Definition (iri,c) ->
      self#print_class iri;
      pp#string " is defined as ";
      self#apply_cls c
    | Instance (iri,c) ->
      self#print_individual iri;
      pp#string " is an instance of ";
      self#apply_cls c
    | Anonymous_instance c ->
      pp#string "there is an instance of ";
      self#apply_cls c
    | New_class iri -> self#print_iri Tag_class iri; pp#string " is a class"
    | New_property iri -> self#print_iri Tag_property iri; pp#string " is a property"
    | New_inverse_property iri -> self#print_iri Tag_property iri; pp#string " is an inverse property"
    | Subclassof (iri1,iri2) -> self#print_iri Tag_class iri1; pp#string " is a subclass of "; self#print_iri Tag_class iri2
    | Subpropertyof (iri1,iri2) -> self#print_iri Tag_property iri1; pp#string " is a subproperty of "; self#print_iri Tag_property iri2
    | Property_inverse (iri,inverse_iri) -> self#print_iri Tag_property inverse_iri; pp#string " is the inverse of "; self#print_iri Tag_property iri
    | Functional prop -> self#print_prop prop; pp#string " is functional"
    | Inverse_functional prop -> self#print_prop prop; pp#string " is inverse functional"
    | Reflexive prop -> self#print_prop prop; pp#string " is reflexive"
    | Irreflexive prop -> self#print_prop prop; pp#string " is irreflexive"
    | Symmetric prop -> self#print_prop prop; pp#string " is symmetric"
    | Asymmetric prop -> self#print_prop prop; pp#string " is asymmetric"
    | Transitive prop -> self#print_prop prop; pp#string " is transisitve"
    | All_disjoint [] -> assert false
    | All_disjoint [_] -> assert false
    | All_disjoint (c::lc) ->
      pp#string "all of "; self#apply_cls c;
      List.iter (fun c -> pp#string ", "; self#apply_cls c) lc;
      pp#string " are disjoint"
	
    val mutable focus_opt : focus option = None
	
    method apply_cls ?(focus : focus option) c =
      focus_opt <- focus;
      self#print_cls ~prec:prec_or (Some [], c)

    method private print_cls ~prec (ps_opt, c) =
      Option.iter (fun ps -> pp#open_tag (Tag_path ps)) ps_opt;
      if ps_opt <> None && focus_opt = ps_opt
      then
	if c = Thing
	then pp#focus
	else
	  print_in_scope prec prec_and
	    (fun pp ->
	      self#print_cls_aux ~prec ps_opt c;
	      pp#newline;
	      pp#focus)
	    pp
      else self#print_cls_aux ~prec ps_opt c;
      Option.iter (fun ps -> pp#close_tag) ps_opt

    method private print_cls_aux ~prec ps_opt c =
      match c with
      | Thing ->
	  self#print_thing
      | Individual iri ->
	  self#print_individual iri
      | Class iri ->
	  self#print_class iri
      | ObjectExists (Inverse (Inverse p), c1) ->
	  self#print_cls ~prec (ps_opt, ObjectExists (p, c1))
      | ObjectExists (ObjectProperty iri, c1) ->
	  print_in_scope prec prec_restr
	    (fun pp ->
	      self#print_object_exists iri (extends_ps ps_opt 0, c1))
	    pp
      | ObjectExists (Inverse (ObjectProperty iri), c1) ->
	  print_in_scope prec prec_restr
	    (fun pp ->
	      self#print_inverse_object_exists iri (extends_ps ps_opt 0, c1))
	    pp
      | Compl c1 ->
	  print_in_scope prec prec_not
	    (fun pp ->
	      self#print_compl (None, c1))
	    pp
      | Inter lc ->
	  ( match lc with
	  | [] -> self#print_cls ~prec (ps_opt, Thing)
	  | [c1] -> self#print_cls ~prec (extends_ps ps_opt 0, c1)
	  | c1::lc1 ->
	      print_in_scope prec prec_and
		(fun pp ->
		  let pc1 = extends_ps ps_opt 0, c1 in
		  let _, rev_lpc1 = List.fold_left (fun (i,rev_lpc1) c1 -> (i+1, (extends_ps ps_opt i, c1)::rev_lpc1)) (1,[]) lc1 in
		  self#print_inter pc1 (List.rev rev_lpc1))
		pp )
      | Union lc ->
	  ( match lc with
	  | [] -> assert false
	  | [c1] -> self#print_cls ~prec (extends_ps ps_opt 0, c1)
	  | c1::lc1 ->
	      print_in_scope prec prec_or
		(fun pp ->
		  let pc1 = extends_ps ps_opt 0, c1 in
		  let _, rev_lpc1 = List.fold_left (fun (i,rev_lpc1) c1 -> (i+1, (extends_ps ps_opt i, c1)::rev_lpc1)) (1,[]) lc1 in
		  self#print_union pc1 (List.rev rev_lpc1))
		pp )

    method private print_prop = function
    | ObjectProperty iri -> self#print_iri Tag_property iri
    | Inverse prop -> pp#string "inverse "; self#print_prop prop

    method virtual private print_thing :  unit
    method virtual private print_individual : iri -> unit
    method virtual private print_class : iri -> unit
    method virtual private print_object_exists : iri -> focus option * cls -> unit
    method virtual private print_inverse_object_exists : iri -> focus option * cls -> unit
    method virtual private print_compl : focus option * cls -> unit
    method virtual private print_inter : focus option * cls -> (focus option * cls) list -> unit
    method virtual private print_union : focus option * cls -> (focus option * cls) list -> unit


    method private print_iri tag iri =
      pp#open_tag tag;
      self#print_iri_bare iri;
      pp#close_tag

    method private print_iri_bare iri =
      let o_iri = IRI.create iri in
      match o_iri#get_fragment with
      | Some s -> pp#string s
      | None -> pp#string o_iri#to_quoted_string

    method print_modifier str =
      pp#open_tag Tag_modifier;
      pp#string str;
      pp#close_tag
	
    method private print_space =
      pp#string " "
  end

let apply_cls ?focus c = fun print -> print#apply_cls ?focus c
let apply_axiom a = fun print -> print#apply_axiom a

class print_lis pp =
  object (self)
    inherit print pp

    method private print_thing =
      pp#string " ? "

    method private print_individual iri =
      self#print_iri Tag_individual iri

    method private print_class iri =
      pp#string "a"; self#print_space; self#print_iri Tag_class iri

    method private print_object_exists iri pc1 =
      self#print_iri Tag_property iri;
      self#print_space;
      self#print_cls ~prec:prec_not pc1

    method private print_inverse_object_exists iri pc1 =
      self#print_cls ~prec:prec_not pc1;
      self#print_space;
      self#print_iri Tag_property iri;
      self#print_space;
      self#print_modifier "this"

    method private print_compl pc1 =
      self#print_modifier "not"; self#print_space;
      self#print_cls ~prec:prec_not pc1

    method private print_inter pc1 lpc1 =
      self#print_cls ~prec:prec_and pc1;
      let _ =
	List.fold_left
	  (fun i pc1 ->
	    pp#newline;
	    self#print_cls ~prec:prec_and pc1;
	    i+1)
	  1 lpc1 in
      ()

    method private print_union pc1 lpc1 =
      self#print_cls ~prec:prec_or pc1;
      let _ =
	List.fold_left
	  (fun i pc1 ->
	    pp#newline; self#print_space; self#print_modifier "or"; self#print_space;
	    self#print_cls ~prec:prec_or pc1;
	    i+1)
	  1 lpc1 in
      ()
  end

let print_lis = fun pp -> new print_lis pp

  
class print_dl pp =
  object (self)
    inherit print pp

    method private print_thing =
      pp#string "\226\138\164" (* \top *)

    method private print_individual iri =
      pp#string "{"; self#print_iri Tag_individual iri; pp#string "}"

    method private print_class iri =
      self#print_iri Tag_class iri

    method private print_object_exists iri pc1 =
      pp#string "\226\136\131 "; (* \exists *)
      self#print_iri Tag_property iri;
      pp#string " . ";
      self#print_cls ~prec:prec_not pc1

    method private print_inverse_object_exists iri pc1 =
      pp#string "\226\136\131 "; (* \exists *)
      self#print_iri Tag_property iri;
      pp#string " \226\129\187 . "; (* ^- *)
      self#print_cls ~prec:prec_not pc1

    method private print_compl pc1 =
      pp#string "\194\172 "; self#print_space; (* \lnot *)
      self#print_cls ~prec:prec_not pc1

    method private print_inter pc1 lpc1 =
      self#print_cls ~prec:prec_and pc1;
      let _ =
	List.fold_left
	  (fun i pc1 ->
	    pp#string " \226\138\147 "; pp#newline; (* \sqcap *)
	    self#print_cls ~prec:prec_and pc1;
	    i+1)
	  1 lpc1 in
      ()

    method private print_union pc1 lpc1 =
      self#print_cls ~prec:prec_or pc1;
      let _ =
	List.fold_left
	  (fun i pc1 ->
	    pp#newline; pp#string " \226\138\148 "; (* \sqcup *)
	    self#print_cls ~prec:prec_or pc1;
	    i+1)
	  1 lpc1 in
      ()
  end

let print_dl = fun pp -> new print_dl pp


class print_manchester pp =
  object (self)
    inherit print pp

    method private print_thing =
      pp#string "Thing" (* \top *)

    method private print_individual iri =
      pp#string "{"; self#print_iri Tag_individual iri; pp#string "}"

    method private print_class iri =
      self#print_iri Tag_class iri

    method private print_object_exists iri pc1 =
      self#print_iri Tag_property iri;
      pp#string " some ";
      self#print_cls ~prec:prec_not pc1

    method private print_inverse_object_exists iri pc1 =
      self#print_modifier "inverse ";
      self#print_iri Tag_property iri;
      pp#string " some ";
      self#print_cls ~prec:prec_not pc1

    method private print_compl pc1 =
      self#print_modifier "not "; self#print_space;
      self#print_cls ~prec:prec_not pc1

    method private print_inter pc1 lpc1 =
      self#print_cls ~prec:prec_and pc1;
      let _ =
	List.fold_left
	  (fun i pc1 ->
	    pp#string " and "; pp#newline;
	    self#print_cls ~prec:prec_and pc1;
	    i+1)
	  1 lpc1 in
      ()

    method private print_union pc1 lpc1 =
      self#print_cls ~prec:prec_or pc1;
      let _ =
	List.fold_left
	  (fun i pc1 ->
	    pp#newline; self#print_modifier " or ";
	    self#print_cls ~prec:prec_or pc1;
	    i+1)
	  1 lpc1 in
      ()
  end

let print_manchester = fun pp -> new print_manchester pp

