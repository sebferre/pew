
open Owlapi
module Expr = Expression

(* printers *)

class virtual ['tag] printer =
  object (self)
    method virtual string : string -> unit
    method virtual image : Uri.t -> unit
    method virtual focus : unit
    method open_tag (x : 'tag) = ()
    method close_tag = ()
    method virtual kwd_and : unit
    method virtual open_scope : unit
    method virtual close_scope : unit
    method break : unit = ()
    method newline : unit = ()
  end

class ['tag] string_printer =
  object (pp)
    inherit ['tag] printer
    val buf : Buffer.t = Buffer.create 255
    method string s = Buffer.add_string buf s
    method image (uri : Uri.t) = ()
    method focus = pp#string "?"
    method kwd_and = pp#string " and "
    method open_scope = pp#string "("
    method close_scope = pp#string ")"
    method contents = Buffer.contents buf
  end

let print_to_string print apply =
  let pp = new string_printer in
  apply (print (pp :> 'tag printer));
  pp#contents


type 'tag print_tree = Str of string | Image of Uri.t | Focus | Tag of 'tag * 'tag print_tree list

class ['tag] string_printer_with_tags =
  object (pp)
    inherit ['tag] printer
    val mutable res : 'tag print_tree list list = [[]]
    val mutable tags : 'tag list = []
    method string s = assert (res <> []); res <- (Str s :: List.hd res) :: List.tl res
    method image uri = assert (res <> []); res <- (Image uri :: List.hd res) :: List.tl res
    method focus = assert (res <> []); res <- (Focus :: List.hd res) :: List.tl res
    method open_tag x = tags <- x::tags; res <- [] :: res
    method close_tag =
      match tags with
      | [] -> failwith "Lisql.string_printer_with_tags#close_tag: unmatched 'close_tag'"
      | x::xs ->
	  tags <- xs;
	  match res with
	  | [] -> assert false
	  | [_] -> assert false
	  | l1::l2::ls ->
	      res <- (Tag (x, List.rev l1)::l2) :: ls
    method kwd_and = pp#string " and "
    method open_scope = pp#string "("
    method close_scope = pp#string ")"
    method contents =
      match res with
      | [] -> assert false
      | [l] -> List.rev l
      | _ -> failwith "Lisqsl.string_printer_with_tags#contents: unmatched 'open_tag'"
  end

let print_to_string_with_tags print apply =
  let pp = new string_printer_with_tags in
  apply (print (pp :> 'tag printer));
  pp#contents

class scope_indent =
  let s_incr, n_incr = "\t", 1 in
  object
    val mutable indent = "\n"
    val mutable start = false
    val mutable offset = 0
    method open_scope =
      indent <- indent ^ s_incr;
      start <- true
    method close_scope =
      try
	indent <- String.sub indent 0 (String.length indent - n_incr);
	start <- true;
	offset <- 0
      with _ -> failwith "Ontology.scope_indent: unmatched scope closing"
    method break =
      if offset > 40 then begin
	start <- true;
	offset <- 0
      end
    method newline =
      start <- true;
      offset <- 0
    method indent =
      if start then begin
	start <- false;
	offset <- String.length indent - 1;
	Some indent end
      else None
    method shift n =
      offset <- offset + n
  end

class ['tag] string_printer_indent =
  object (pp)
    inherit ['tag] string_printer as super
    val scope = new scope_indent
    method private indent = Option.iter super#string scope#indent
    method string s = pp#indent; super#string s; scope#shift (String.length s)
    method focus = pp#indent; super#focus
    method kwd_and = ()
    method open_scope = scope#open_scope
    method close_scope = scope#close_scope
    method break = scope#break
    method newline = scope#newline
  end

let print_to_string_indent print apply =
  let pp = new string_printer_indent in
  apply (print (pp :> 'tag printer));
  pp#contents

class ['tag] string_printer_indent_with_tags = (* with tags for queries *)
  object (pp)
    inherit ['tag] string_printer_with_tags as super
    val scope = new scope_indent
    method private indent = Option.iter super#string scope#indent
    method string s = pp#indent; super#string s; scope#shift (String.length s)
    method focus = pp#indent; super#focus
    method kwd_and = ()
    method open_scope = scope#open_scope
    method close_scope = scope#close_scope
    method break = scope#break
    method newline = scope#newline
  end

let print_to_string_indent_with_tags print apply =
  let pp = new string_printer_indent_with_tags in
  apply (print (pp :> 'tag printer));
  pp#contents


let string_of_axiom print (a : Expr.axiom) : string =
  print_to_string print (Expr.apply_axiom a)

let string_of_class print (c : Expr.cls) : string =
  print_to_string_indent print (Expr.apply_cls c)
    
let tree_of_class print (c : Expr.cls) : Expr.print_tag print_tree list =
  print_to_string_indent_with_tags print (Expr.apply_cls c)
    
let tree_of_focus_class print (ps, c : Expr.focus_cls) : Expr.print_tag print_tree list =
  print_to_string_indent_with_tags print (Expr.apply_cls ~focus:ps c)

let rec get_subexpressions store : Expr.cls -> Expr.cls list =
  function
  | Expr.Thing ->
      get_instances ~direct:true store (store#data_factory#get_thing :> ClassExpression.obj) @
      get_direct_subclasses store (store#data_factory#get_thing :> ClassExpression.obj) @
      get_direct_subproperties store (store#data_factory#get_top_object_property :> ObjectPropertyExpression.obj)
  | Expr.Class iri ->
      get_direct_subclasses store (store#data_factory#get_class (IRI.create iri) :> ClassExpression.obj) @
      get_instances ~direct:true store (store#data_factory#get_class (IRI.create iri) :> ClassExpression.obj)
  | Expr.ObjectExists (prop, Expr.Thing) ->
      get_direct_subproperties store (Expr.owlapi_of_prop store#data_factory prop) @
	List.map (fun cls -> Expr.ObjectExists (prop, cls)) (get_instances ~direct:true store (store#data_factory#get_thing :> ClassExpression.obj)) @
	List.map (fun cls -> Expr.ObjectExists (prop, cls)) (get_direct_subclasses store (store#data_factory#get_thing :> ClassExpression.obj))
  | Expr.ObjectExists (prop, Expr.Class iri) ->
      List.map (fun cls -> Expr.ObjectExists (prop, cls)) (get_direct_subclasses store (store#data_factory#get_class (IRI.create iri) :> ClassExpression.obj)) @
      List.map (fun cls -> Expr.ObjectExists (prop, cls)) (get_instances ~direct:true store (store#data_factory#get_class (IRI.create iri) :> ClassExpression.obj))
  | _ -> []
and get_direct_subclasses store ce =
  let nodeset : Class.obj NodeSet.obj = store#reasoner#get_subclasses ce true in
  nodeset#fold
    (fun res node ->
      if node#is_bottom_node
      then res
      else
	let c : Class.obj = node#get_representative_element in
	Expr.Class c#get_iri#to_string ::
	res)
    []
and get_direct_subproperties store (ope : ObjectPropertyExpression.obj) =
  let nodeset : ObjectPropertyExpression.obj NodeSet.obj = store#reasoner#get_object_subproperties ope true in
  nodeset#fold
    (fun res node ->
      if node#is_bottom_node
      then res
      else
	let ope : ObjectPropertyExpression.obj =
	  node#fold
	    (fun res ope ->
	      if ope#is_inverse
	      then res
	      else ope)
	    node#get_representative_element in
	let prop = Expr.ObjectProperty ope#get_iri#to_string in
	let prop = if ope#is_inverse then Expr.Inverse prop else prop in
	Expr.ObjectExists (prop, Expr.Thing) ::
	res)
    []
and get_instances ~(direct : bool) store (ce : ClassExpression.obj) =
  let nodeset : Individual.obj NodeSet.obj = store#reasoner#get_instances ce direct in
  nodeset#fold
    (fun res node ->
      let ind : Individual.obj = node#get_representative_element in
      match ind#get_iri_opt with
      | Some iri -> Expr.Individual iri#to_string :: res
      | None -> res)
    []

type link =
    { link_cls : Expr.cls;
      link_necessary : bool
    }

type axiom_status = AlreadyTrue | OK | Tautology | HasInstance

class place store (ps_c : Expr.focus_cls) =
  let ps, c = ps_c in
  let c1, ck = Expr.get_reverse_context ps_c in
  let c1k = Expr.simpl_inter c1 ck in
  let compl_c1k = Expr.simpl_inter (Expr.Compl c1) ck in
  let ce = Expr.owlapi_of_cls store#data_factory c1k in
  let neg_ce = Expr.owlapi_of_cls store#data_factory compl_c1k in
  object (self)
    method is_satisfiable : bool =
      store#reasoner#is_satisfiable ce

    method is_necessary : bool =
      not (store#reasoner#is_satisfiable (store#data_factory#get_object_complement_of ce))
	(*
      store#reasoner#is_entailed
	(store#data_factory#get_subclass_of
	   (store#data_factory#get_thing :> ClassExpression.obj)
	   ce)
	*)

    method is_necessary_at_focus : bool =
      not (store#reasoner#is_satisfiable neg_ce)

    method has_instance : bool =
      not (store#reasoner#get_instances ce false)#is_empty
 (* to check whether (ce subClassOf Nothing) leads to an inconsistency *)
(*
      not (store#reasoner#is_satisfiable
	     (store#data_factory#get_object_complement_of
		(store#data_factory#get_object_some_values_from
		   (store#data_factory#get_top_object_property :> ObjectPropertyExpression.obj)
		   ce)))
*)

    method has_link (x : Expr.cls) : bool =
      let xe = Expr.owlapi_of_cls store#data_factory x in
      let sat =
	store#reasoner#is_satisfiable 
	  (store#data_factory#get_object_intersection_of [ce; xe]) in
      let sat =
	let c1kx = Expr.simpl_inter c1k x in
	Expr.fold_focus
	  (fun sat prefix psi ki ci ->
	    match ki with
	    | (n, Expr.Union l)::ki' ->
		sat && (new place store (psi, c1kx))#is_satisfiable
	    | _ -> sat)
	  sat [] [] c1kx in
      sat

    method get_links (x : Expr.cls) : link list =
      let xe = Expr.owlapi_of_cls store#data_factory x in
      let sat = self#has_link x in
      if sat
      then
	let nec =
	  store#reasoner#is_entailed
	    (store#data_factory#get_subclass_of ce xe) in
	if nec
	then [ { link_cls = x; link_necessary = true } ]
	else [ { link_cls = x; link_necessary = false };
	       { link_cls = Expr.Compl x; link_necessary = false } ]
      else []

    method get_sublinks (x : Expr.cls) : link list =
      List.fold_left
	(fun res y -> self#get_links y @ res)
	[] (get_subexpressions store x)

    method get_instances : link list =
      List.fold_left
	(fun res y -> [ { link_cls = y; link_necessary = false } ] (*self#get_links y*) @ res)
	[] (get_instances ~direct:false store ce (*store#data_factory#get_thing :> ClassExpression.obj*))

(*
    method declare_class (iri : Expr.iri) : unit =
(*      let ax = store#data_factory#get_declaration (store#data_factory#get_class (IRI.create iri) :> OWLEntity.obj) in *)
      let ax =
	store#data_factory#get_subclass_of
	  (store#data_factory#get_class (IRI,create iri))
	  (store#data_factory#get_thing :> ClassExpression.obj) in
      store#add_axiom ax

    method declare_object_property (iri : Expr.iri) : unit =
      let ax = store#data_factory#get_declaration (store#data_factory#get_object_property (IRI.create iri) :> OWLEntity.obj) in
      store#add_axiom ax

    method declare_individual (iri : Expr.iri) : unit =
(*      let ax = store#data_factory#get_declaration (store#data_factory#get_named_individual (IRI.create iri) :> OWLEntity.obj) in *)
      let ax =
	store#data_factory#get_class_assertion
	  (store#data_factory#get_thing :> ClassExpression.obj)
	  (store#data_factory#get_named_individual (IRI.create iri) :> Individual.obj) in
      store#add_axiom ax
*)

    method make_possible : unit =
      store#add_axiom_possible (Expr.Possible c) c

    method make_necessary_at_focus : axiom_status =
      if self#is_necessary_at_focus then AlreadyTrue
      else store#add_axiom_necessary (Expr.Necessary (ck,c1)) ck c1

    method make_all_necessary (lx : Expr.cls list) : axiom_status =
      store#add_axiom_necessary
	(Expr.All_necessary (c1k,lx))
	c1k (Expr.Inter lx)

    method make_impossible : axiom_status =
      store#add_axiom_impossible (Expr.Impossible c) c

    method make_all_impossible (lx : Expr.cls list) : axiom_status =
      store#add_axiom_impossible
	(Expr.All_impossible (c1k, lx))
	(Expr.simpl_inter c1k (Expr.simpl_union lx))

    method name (iri : Expr.iri) : unit =
      store#define_class iri c

    method add_instance (iri : Expr.iri) : unit =
      store#add_instance iri c

    method add_anonymous_instance : unit =
      store#add_anonymous_instance c

  end

class store (filename : string) (manager : OntologyManager.obj) (ontology : Ontology.obj) =
  let data_factory = manager#get_data_factory in
  let reasoner_factory = ReasonerFactory.create () in
  let reasoner = reasoner_factory#create_reasoner ontology in
  let renamer = EntityRenamer.create manager (JTreeSet.from_list (new Ontology.obj) [ontology]) in
  let base = filename ^ "#" in
  let out_log = open_out_gen [Open_creat; Open_text; Open_append] 0o644 (Filename.chop_extension filename ^ ".pew") in
  let start_time = Unix.time () in
  object (store)
    method data_factory = data_factory
    method reasoner = reasoner

    val mutable base : Uri.t = base
    val mutable xmlns : Rdf.xmlns = 
      [ (":", "#"); (* default namespace *)
	(Xsd.prefix, Xsd.namespace);
	(Rdf.prefix, Rdf.namespace);
	(Rdfs.prefix, Rdfs.namespace);
	(Owl.prefix, Owl.namespace)
      ]

    method base = base
    method xmlns = xmlns

    method set_base uri =
      if uri <> base
      then base <- uri
    method add_prefix pre uri =
      if not (List.mem (pre,uri) xmlns)
      then xmlns <- (pre,uri)::List.remove_assoc pre xmlns

(*
    method set_label uri label = ()
    method set_inverseLabel uri inv_label = ()
*)

    method log line =
      output_string out_log (string_of_int (int_of_float (Unix.time () -. start_time)));
      output_string out_log "\t";
      output_string out_log line;
      output_string out_log "\n";
      flush out_log
    initializer
      store#log "SESSION START"

    val mutable callbacks_signature_changed : (unit -> unit) list = []
    method when_signature_changed ~callback = callbacks_signature_changed <- callback::callbacks_signature_changed
    method private signature_changed = List.iter (fun callback -> callback ()) callbacks_signature_changed

    method rename (iri : Expr.iri) (new_iri : Expr.iri) =
      renamer#change_iri (IRI.create iri) (IRI.create new_iri);
      reasoner#flush;
      store#log (string_of_axiom Expr.print_manchester (Expr.Rename (iri,new_iri)));
      store#signature_changed

    val mutable axiom_stack : (Expr.axiom * Axiom.obj) list = []

    method axiom_list = List.map fst axiom_stack

    method add_axiom (expr : Expr.axiom) (ax : Axiom.obj) : unit =
      manager#add_axiom ontology ax;
      reasoner#flush;
      store#log (string_of_axiom Expr.print_manchester expr);
      axiom_stack <- (expr,ax)::axiom_stack

    method remove_axiom (expr : Expr.axiom) : unit =
      try
	let ax = List.assoc expr axiom_stack in
	manager#remove_axiom ontology ax;
	reasoner#flush;
	axiom_stack <- List.remove_assoc expr axiom_stack
      with Not_found -> ()

    method undo_axiom : unit =
      match axiom_stack with
      | [] -> ()
      | (_,ax)::l ->
	manager#remove_axiom ontology ax;
	reasoner#flush;
	axiom_stack <- l

    method add_axiom_possible (axiom : Expr.axiom) (c : Expr.cls) : unit =
      let ax = store#data_factory#get_subclass_of
	(Expr.owlapi_of_cls data_factory c)
	(store#data_factory#get_thing :> ClassExpression.obj) in
      store#add_axiom axiom ax
    method add_axiom_impossible (axiom : Expr.axiom) (c : Expr.cls) : axiom_status =
      let ce = Expr.owlapi_of_cls data_factory c in
      if not (store#reasoner#is_satisfiable (store#data_factory#get_object_complement_of ce)) then Tautology
      else if not ((store#reasoner#get_instances ce false)#is_empty) then HasInstance
      else begin
	let ax = store#data_factory#get_subclass_of ce (store#data_factory#get_nothing :> ClassExpression.obj) in
	store#add_axiom axiom ax;
	OK end
    method add_axiom_necessary (axiom : Expr.axiom) (c1 : Expr.cls) (c2 : Expr.cls) : axiom_status =
      store#add_axiom_impossible axiom (Expr.simpl_inter c1 (Expr.Compl c2))

    method make_subsumed_by (c1 : Expr.cls) (c2 : Expr.cls) : axiom_status =
      let open Expr in
      let rec aux_prop prop1 prop2 = (* special handling of existential restriction, interpreted as properties *)
	match prop1, prop2 with
	| ObjectProperty iri1, ObjectProperty iri2 -> store#add_subproperty_of iri1 iri2; OK
	| Inverse p1, Inverse p2 -> aux_prop p1 p2
	| _ -> OK (* invalid *)
      in
      match c1, c2 with
      | Individual iri1, _ -> store#add_instance iri1 c2; OK
      | Class iri1, Class iri2 -> store#add_subclass_of iri1 iri2; OK
      | ObjectExists (prop1,Thing), ObjectExists (prop2,Thing) -> aux_prop prop1 prop2
      | _ -> store#add_axiom_necessary (Expr.Necessary (c1,c2)) c1 c2
	
    method add_class (iri : Expr.iri) : unit =
      store#add_axiom_possible (Expr.New_class iri) (Expr.Class iri);
      store#signature_changed
    method add_property (iri : Expr.iri) : unit =
      store#add_axiom_possible (Expr.New_property iri) (Expr.ObjectExists (Expr.ObjectProperty iri, Expr.Thing));
      store#signature_changed
	  
    method add_subclass_of (child : Expr.iri) (parent : Expr.iri) : unit =
      let ax = data_factory#get_subclass_of
	(Expr.owlapi_of_cls data_factory (Expr.Class child))
	(Expr.owlapi_of_cls data_factory (Expr.Class parent)) in
      store#add_axiom (Expr.Subclassof (child,parent)) ax;
      store#signature_changed
    method add_subproperty_of (child : Expr.iri) (parent : Expr.iri) : unit =
      let ax = data_factory#get_subobjectproperty_of
	(Expr.owlapi_of_prop data_factory (Expr.ObjectProperty child))
	(Expr.owlapi_of_prop data_factory (Expr.ObjectProperty parent)) in
      store#add_axiom (Expr.Subpropertyof (child,parent)) ax;
      store#signature_changed
    method add_property_inverse (iri : Expr.iri) (inverse_iri : Expr.iri) : unit =
      let ax = data_factory#get_inverse_object_properties
	(Expr.owlapi_of_prop data_factory (Expr.ObjectProperty iri))
	(Expr.owlapi_of_prop data_factory (Expr.ObjectProperty inverse_iri)) in
      store#add_axiom (Expr.Property_inverse (iri,inverse_iri)) ax;
      store#signature_changed
	
    method make_functional (prop : Expr.prop) : unit =
      let ax = data_factory#get_functional_object_property
	(Expr.owlapi_of_prop data_factory prop) in
      store#add_axiom (Expr.Functional prop) ax
    method make_inverse_functional (prop : Expr.prop) : unit =
      let ax = data_factory#get_inverse_functional_object_property
	(Expr.owlapi_of_prop data_factory prop) in
      store#add_axiom (Expr.Inverse_functional prop) ax
    method make_reflexive (prop : Expr.prop) : unit =
      let ax = data_factory#get_reflexive_object_property
	(Expr.owlapi_of_prop data_factory prop) in
      store#add_axiom (Expr.Reflexive prop) ax
    method make_irreflexive (prop : Expr.prop) : unit =
      let ax = data_factory#get_irreflexive_object_property
	(Expr.owlapi_of_prop data_factory prop) in
      store#add_axiom (Expr.Irreflexive prop) ax
    method make_symmetric (prop : Expr.prop) : unit =
      let ax = data_factory#get_symmetric_object_property
	(Expr.owlapi_of_prop data_factory prop) in
      store#add_axiom (Expr.Symmetric prop) ax
    method make_asymmetric (prop : Expr.prop) : unit =
      let ax = data_factory#get_asymmetric_object_property
	(Expr.owlapi_of_prop data_factory prop) in
      store#add_axiom (Expr.Asymmetric prop) ax
    method make_transitive (prop : Expr.prop) : unit =
      let ax = data_factory#get_transitive_object_property
	(Expr.owlapi_of_prop data_factory prop) in
      store#add_axiom (Expr.Transitive prop) ax

    method define_class (iri : Expr.iri) (c : Expr.cls) : unit =
      let ax = store#data_factory#get_equivalent_classes
	(store#data_factory#get_class (IRI.create iri) :> ClassExpression.obj)
	(Expr.owlapi_of_cls store#data_factory c) in
      store#add_axiom (Expr.Definition (iri,c)) ax;
      store#signature_changed
    method add_instance (iri : Expr.iri) (c : Expr.cls) : unit =
      let ax = store#data_factory#get_class_assertion
	(Expr.owlapi_of_cls store#data_factory c)
	(store#data_factory#get_named_individual (IRI.create iri) :> Individual.obj) in
      store#add_axiom (Expr.Instance (iri,c)) ax;
      store#signature_changed
    method add_anonymous_instance (c : Expr.cls) : unit =
      let ax = store#data_factory#get_class_assertion
	(Expr.owlapi_of_cls store#data_factory c)
	(store#data_factory#get_anonymous_individual :> Individual.obj) in
      store#add_axiom (Expr.Anonymous_instance c) ax

    method make_all_disjoint (lx : Expr.cls list) : axiom_status =
      let rec fold_all_pairs f init l =
	match l with
	| [] -> init
	| x::r ->
	  fold_all_pairs f
	    (List.fold_left (fun res y -> f res x y) init r)
	    r
      in
      if List.length lx >= 2
      then
	let c =
	  Expr.simpl_union
	    (fold_all_pairs
	       (fun res x y -> Expr.Inter [x;y] :: res)
	       [] lx) in
	store#add_axiom_impossible (Expr.All_disjoint lx) c
      else OK

    method save : unit =
      manager#save_ontology ontology

    method save_as (path : string) : unit =
      let file = JFile.create_from_path path in
      let iri = IRI.create_from_file file in
      manager#save_ontology_to_iri ontology iri
(*
      let out = JFileOutputStream.create_from_file file in
      manager#save_ontology_to_output_stream ontology (out :> JOutputStream.obj)
*)

    method place (ps_c : Expr.focus_cls) : place =
      new place store ps_c

    method restrictions : Expr.cls list =
      let l = [] in
      let l =
	ontology#get_individuals#fold
	  (fun l i ->
	    let cls = Expr.Individual i#get_iri#to_string in
	    cls :: Expr.Compl cls :: l)
	  l in
      let l =
	ontology#get_classes#fold
	  (fun l c ->
	    let cls = Expr.Class c#get_iri#to_string in
	    cls :: Expr.Compl cls :: l)
	  l in
      let l =
	ontology#get_object_properties#fold
	  (fun l p ->
	    let iri = p#get_iri#to_string in
	    let prop = Expr.ObjectProperty iri in
	    let cls_fwd = Expr.ObjectExists (prop, Expr.Thing) in
	    let cls_bwd = Expr.ObjectExists (Expr.Inverse prop, Expr.Thing) in
	    cls_fwd :: Expr.Compl cls_fwd :: cls_bwd :: Expr.Compl cls_bwd :: l)
	  l in
      l
  end

(*
let from_scratch uri =
  let manager : OntologyManager.obj = Manager.create_OntologyManager () in
  let iri = IRI.create uri in
  let ontology : Ontology.obj = manager#create_ontology in
  new store manager ontology (uri ^ "#")

let from_uri uri =
  let manager : OntologyManager.obj = Manager.create_OntologyManager () in
  let iri : IRI.obj = IRI.create uri in
  let ontology : Ontology.obj = manager#load_ontology_from_iri iri in
  new store manager ontology (uri ^ "#")
*)
    
let from_path path =
  let manager : OntologyManager.obj = Manager.create_OntologyManager () in
  let file : JFile.obj = JFile.create_from_path path in
  let ontology : Ontology.obj = manager#load_ontology_from_file file in
  new store path manager ontology

