
type typ = V | I | Z | L of string | Array of typ

let rec string_of_typ = function
  | V -> "V"
  | I -> "I"
  | Z -> "Z"
  | L path -> "L" ^ path ^ ";"
  | Array t -> "[" ^ string_of_typ t

let signature (args : typ list) (res : typ) : string =
  "(" ^ String.concat "" (List.map string_of_typ args) ^ ")" ^ string_of_typ res

module JString =
  struct
    let path = "java/lang/String"

    class obj (this : Jni.obj) =
      object
	method to_string = Common.prof "Owlapi.JString.obj#to_string" (fun () ->
	  Jni.string_from_java this)
      end

    let create (s : string) = Common.prof "Owlapi.JString.create" (fun () ->
      new obj (Jni.string_to_java s))
  end

module JObject =
  struct
    let path = "java/lang/Object"
    let c =
      try Jni.find_class path
      with _ -> failwith "Owlapi: class Object could not be found"
    let m_toString =
      try Jni.get_methodID c
	  "toString" (signature [] (L JString.path))
      with _ -> failwith "Owlapi: method Object.toString could not be found"

    class obj (this : Jni.obj) =
      object
	method jni : Jni.obj = this

	method to_string : string = Common.prof "Owlapi.JObject.obj#to_string" (fun () ->
	  Jni.string_from_java
	    (try Jni.call_object_method this m_toString [||]
	    with _ -> failwith "Owlapi.JObject#to_string: unknown error"))
      end

    let is_same_object o1 o2 = Common.prof "Owlapi.JObject.is_same_object" (fun () ->
      Jni.is_same_object o1#jni o2#jni)
  end

let debug f =
  try f ()
  with Jni.Exception o ->
    failwith (new JObject.obj o)#to_string

let find_class path = debug (fun () -> Jni.find_class path)
let get_methodID c m s = debug (fun () -> Jni.get_methodID c m s)
let get_static_methodID c m s = debug (fun () -> Jni.get_static_methodID c m s)
let call_static_object_method c m args = debug (fun () -> Jni.call_static_object_method c m args)
let call_object_method this m args = debug (fun () -> Jni.call_object_method this m args)
let call_nonvirtual_void_method this c init args = debug (fun () -> Jni.call_nonvirtual_void_method this c init args)
let call_void_method this m args = debug (fun () -> Jni.call_void_method this m args)
let call_boolean_method this m args = debug (fun () -> Jni.call_boolean_method this m args)

module Iterator =
  struct
    let path = "java/util/Iterator"
    let c = find_class path
    let m_hasNext = get_methodID c
	"hasNext" (signature [] Z)
    let m_next = get_methodID c
	"next" (signature [] (L JObject.path))

    class ['e] obj (make_e : Jni.obj -> 'e) (this : Jni.obj) =
      object
	inherit JObject.obj this

	method has_next = Common.prof "Owlapi.Iterator.obj#has_next" (fun () ->
	  call_boolean_method this m_hasNext [||])

	method next = Common.prof "Owlapi.Iterator.obj#next" (fun () ->
	  make_e (call_object_method this m_next [||]))
      end
  end

module Iterable =
  struct
    let path = "java/lang/Iterable"
    let c = find_class path
    let m_iterator = get_methodID c
	"iterator" (signature [] (L Iterator.path))

    class ['e] obj (make_e : Jni.obj -> 'e) (this : Jni.obj) =
      object (self)
	method iterator = Common.prof "Owlapi.Iterable.obj#iterator" (fun () ->
	  new Iterator.obj make_e (call_object_method this m_iterator [||]))

	method fold : 'a. ('a -> 'e -> 'a) -> 'a -> 'a =
	  fun f init ->
	    let iter = self#iterator in
	    let res = ref init in
	    while iter#has_next do
	      res := f !res iter#next
	    done;
	    !res

	method is_empty : bool =
	  not self#iterator#has_next
      end
  end

module JList =
  struct
    let path = "java/util/List"
  end

module JSet =
  struct
    let path = "java/util/Set"
    let c = find_class path
    let m_add = get_methodID c
	"add" (signature [L JObject.path] Z)
    
    class ['e] obj (make_e : Jni.obj -> 'e) (this : Jni.obj) =
      object
	inherit JObject.obj this
	inherit ['e] Iterable.obj make_e this

	method add (e : 'e) = Common.prof "Owlapi.JSet.obj#add" (fun () ->
	  call_boolean_method this m_add [|Jni.Obj e#jni|])
      end
  end

module JTreeSet =
  struct
    let path = "java/util/TreeSet"
    let c = find_class path
    let init = get_methodID c
	"<init>" (signature [] V)

    class ['e] obj (make_e : Jni.obj -> 'e) (this : Jni.obj) =
      object
	inherit ['e] JSet.obj make_e this
      end

    let create (make_e : Jni.obj -> 'e) () : 'e obj = Common.prof "Owlapi.JTreeSet.create" (fun () ->
      let this = Jni.alloc_object c in
      call_nonvirtual_void_method this c init [||];
      new obj make_e this)

    let from_list (make_e : Jni.obj -> 'e) (l : 'e list) : 'e obj = Common.prof "Owlapi.JTreeSet.from_list" (fun () ->
      let oset = create make_e () in
      List.iter (fun e -> ignore (oset#add e)) l;
      oset)
  end

module URI =
  struct
    let path = "java/net/URI"

    class obj (this : Jni.obj) =
      object
	inherit JObject.obj this
      end
  end

module JFile =
  struct
    let path = "java/io/File"
    let c = find_class path
    let init = get_methodID c
	"<init>" (signature [L JString.path] V)
    let m_toURI = get_methodID c
	"toURI" (signature [] (L URI.path))

    class obj (this : Jni.obj) =
      object
	inherit JObject.obj this

	method to_uri = Common.prof "Owlapi.JFile.obj#to_uri" (fun () ->
	  new URI.obj (call_object_method this m_toURI [||]))
      end

    let create_from_path (path : string) = Common.prof "Owlapi.JFile.create_from_path" (fun () ->
      let this = Jni.alloc_object c in
      call_nonvirtual_void_method this c init [|Jni.Obj (Jni.string_to_java path)|];
      new obj this)
  end

module JOutputStream =
  struct
    let path = "java/io/OutputStream"

    class obj (this : Jni.obj) =
      object
	inherit JObject.obj this
      end
  end

module JFileOutputStream =
  struct
    let path = "java/io/FileOutputStream"
    let c = find_class path
    let init = get_methodID c
	"<init>" (signature [L JFile.path] V)

    class obj (this : Jni.obj) =
      object
	inherit JOutputStream.obj this
      end

    let create_from_file (file : JFile.obj) = Common.prof "Owlapi.JFileOutputStream.create_from_file" (fun () ->
      let this = Jni.alloc_object c in
      call_nonvirtual_void_method this c init [|Jni.Obj file#jni|];
      new obj this)
  end

module IRI =
  struct
    let path = "org/semanticweb/owlapi/model/IRI"
    let c = find_class path
    let m_create_from_string = get_static_methodID c
	"create" (signature [L JString.path] (L path))
    let m_create_from_file = get_static_methodID c
	"create" (signature [L JFile.path] (L path))
    let m_getFragment = get_methodID c
	"getFragment" (signature [] (L JString.path))
    let m_toQuotedString = get_methodID c
	"toQuotedString" (signature [] (L JString.path))

    class obj (this : Jni.obj) =
      object
	inherit JObject.obj this

	method get_fragment : string option = Common.prof "Owlapi.IRI.obj#get_fragment" (fun () ->
	  let o = call_object_method this m_getFragment [||] in
	  if Jni.is_null o
	  then None
	  else Some (Jni.string_from_java o))

	method to_quoted_string : string = Common.prof "Owlapi.IRI.obj#to_quoted_string" (fun () ->
	  Jni.string_from_java (call_object_method this m_toQuotedString [||]))
      end

    let create (s : string) = Common.prof "Owlapi.IRI.create" (fun () ->
      new obj (call_static_object_method c m_create_from_string [|Jni.Obj (Jni.string_to_java s)|]))

    let create_from_file (file : JFile.obj) = Common.prof "Owlapi.IRI.create_from_file" (fun () ->
      new obj (call_static_object_method c m_create_from_file [|Jni.Obj file#jni|]))
  end

module OWLObject =
  struct
    let path = "org/semanticweb/owlapi/model/OWLObject"

    class obj (this : Jni.obj) =
      object
	inherit JObject.obj this
      end
  end

module OWLNamedObject =
  struct
    let path = "org/semanticweb/owlapi/model/OWLNamedObject"
    let c = find_class path
    let m_getIRI = get_methodID c
	"getIRI" (signature [] (L IRI.path))

    class obj (this : Jni.obj) =
      object
	inherit OWLObject.obj this

	method get_iri = Common.prof "Owlapi.OWLNamedObject.obj#get_iri" (fun () ->
	  new IRI.obj (call_object_method this m_getIRI [||]))
      end
  end

module OWLEntity =
  struct
    let path = "org/semanticweb/owlapi/model/OWLEntity"    

    class obj (this : Jni.obj) =
      object
	inherit OWLNamedObject.obj this
      end
  end

module Individual =
  struct
    let path_NamedIndividual = "org/semanticweb/owlapi/model/OWLNamedIndividual"
    let path = "org/semanticweb/owlapi/model/OWLIndividual"
    let c = find_class path
    let m_asOWLNamedIndividual = get_methodID c
	"asOWLNamedIndividual" (signature [] (L path_NamedIndividual))
    
    class obj (this : Jni.obj) =
      object
	inherit OWLObject.obj this

	method get_iri_opt : IRI.obj option = Common.prof "Owlapi.Individual.obj#get_iri_opt" (fun () ->
	  try
	    let o = new OWLNamedObject.obj (call_object_method this m_asOWLNamedIndividual [||]) in
	    Some o#get_iri
	  with _ -> None)
      end
  end

module NamedIndividual =
  struct
    let path = "org/semanticweb/owlapi/model/OWLNamedIndividual"

    class obj (this : Jni.obj) =
      object
	inherit Individual.obj this
	inherit OWLNamedObject.obj this
      end
  end

module AnonymousIndividual =
  struct
    let path = "org/semanticweb/owlapi/model/OWLAnonymousIndividual"

    class obj (this : Jni.obj) =
      object
	inherit Individual.obj this
      end
  end

module AnnotationProperty =
  struct
    let path = "org/semanticweb/owlapi/model/OWLAnnotationProperty"
    let c = find_class path

    class obj (this : Jni.obj) =
      object
	inherit OWLNamedObject.obj this
      end
  end

module ClassExpression =
  struct
    let path = "org/semanticweb/owlapi/model/OWLClassExpression"
    let c = find_class path
    
    class obj (this : Jni.obj) =
      object
	inherit JObject.obj this
      end
  end

module Class =
  struct
    let path = "org/semanticweb/owlapi/model/OWLClass"
    let c = find_class path
    
    class obj (this : Jni.obj) =
      object
	inherit OWLNamedObject.obj this
	inherit ClassExpression.obj this
      end
  end

module ObjectAllValuesFrom =
  struct
    let path = "org/semanticweb/owlapi/model/OWLObjectAllValuesFrom"
  end

module ObjectComplementOf =
  struct
    let path = "org/semanticweb/owlapi/model/OWLObjectComplementOf"
  end

module ObjectHasValue =
  struct
    let path = "org/semanticweb/owlapi/model/OWLObjectHasValue"
  end

module ObjectIntersectionOf =
  struct
    let path = "org/semanticweb/owlapi/model/OWLObjectIntersectionOf"
  end

module ObjectOneOf =
  struct
    let path = "org/semanticweb/owlapi/model/OWLObjectOneOf"
  end

module ObjectSomeValuesFrom =
  struct
    let path = "org/semanticweb/owlapi/model/OWLObjectSomeValuesFrom"
  end

module ObjectUnionOf =
  struct
    let path = "org/semanticweb/owlapi/model/OWLObjectUnionOf"
  end

module ObjectPropertyExpression =
  struct
    let path_ObjectProperty = "org/semanticweb/owlapi/model/OWLObjectProperty"
    let path = "org/semanticweb/owlapi/model/OWLObjectPropertyExpression"
    let c = find_class path
    let m_getNamedProperty = get_methodID c
	"getNamedProperty" (signature [] (L path_ObjectProperty))
    let m_asOWLObjectProperty = get_methodID c
	"asOWLObjectProperty" (signature [] (L path_ObjectProperty))

    class obj (this : Jni.obj) =
      object
	inherit JObject.obj this

	method get_iri = Common.prof "Owlapi.ObjectPropertyExpression.obj#get_iri" (fun () ->
	  let o = new OWLNamedObject.obj (call_object_method this m_getNamedProperty [||]) in
	  o#get_iri)

	method is_inverse = Common.prof "Owlapi.ObjectPropertyExpression.obj#is_inverse" (fun () ->
	  try ignore (call_object_method this m_asOWLObjectProperty [||]); false
	  with _ -> true)
      end
  end

module ObjectProperty =
  struct
    let path = "org/semanticweb/owlapi/model/OWLObjectProperty"
    let c = find_class path

    class obj (this : Jni.obj) =
      object
	inherit OWLNamedObject.obj this
	inherit ObjectPropertyExpression.obj this
      end
  end

module ObjectInverseOf =
  struct
    let path = "org/semanticweb/owlapi/model/OWLObjectInverseOf"
  end


module Axiom =
  struct
    let path = "org/semanticweb/owlapi/model/OWLAxiom"
    let c = find_class path
    
    class obj (this : Jni.obj) =
      object
	inherit JObject.obj this
      end
  end

module Declaration =
  struct
    let path = "org/semanticweb/owlapi/model/OWLDeclarationAxiom"
  end

module EquivalentClasses =
  struct
    let path = "org/semanticweb/owlapi/model/OWLEquivalentClassesAxiom"
  end

module SubClassOf =
  struct
    let path = "org/semanticweb/owlapi/model/OWLSubClassOfAxiom"
  end

module SubObjectPropertyOf =
  struct
    let path = "org/semanticweb/owlapi/model/OWLSubObjectPropertyOfAxiom"
  end
    
module ClassAssertion =
  struct
    let path = "org/semanticweb/owlapi/model/OWLClassAssertionAxiom"
  end

module DisjointClasses =
  struct
    let path = "org/semanticweb/owlapi/model/OWLDisjointClassesAxiom"
  end

module InverseObjectPropertiesAxiom =
  struct
    let path = "org/semanticweb/owlapi/model/OWLInverseObjectPropertiesAxiom"
  end
    
module FunctionalObjectProperty =
  struct
    let path = "org/semanticweb/owlapi/model/OWLFunctionalObjectPropertyAxiom"
  end
    
module InverseFunctionalObjectProperty =
  struct
    let path = "org/semanticweb/owlapi/model/OWLInverseFunctionalObjectPropertyAxiom"
  end
    
module ReflexiveObjectProperty =
  struct
    let path = "org/semanticweb/owlapi/model/OWLReflexiveObjectPropertyAxiom"
  end

module IrreflexiveObjectProperty =
  struct
    let path = "org/semanticweb/owlapi/model/OWLIrreflexiveObjectPropertyAxiom"
  end

module SymmetricObjectProperty =
  struct
    let path = "org/semanticweb/owlapi/model/OWLSymmetricObjectPropertyAxiom"
  end

module AsymmetricObjectProperty =
  struct
    let path = "org/semanticweb/owlapi/model/OWLAsymmetricObjectPropertyAxiom"
  end

module TransitiveObjectProperty =
  struct
    let path = "org/semanticweb/owlapi/model/OWLTransitiveObjectPropertyAxiom"
  end

module DataFactory =
  struct
    let path = "org/semanticweb/owlapi/model/OWLDataFactory"
    let c = find_class path
    let m_getOWLClass = get_methodID c
	"getOWLClass" (signature [L IRI.path] (L Class.path))
    let m_getOWLNamedIndividual = get_methodID c
      "getOWLNamedIndividual" (signature [L IRI.path] (L NamedIndividual.path))
    let m_getOWLAnonymousIndividual = get_methodID c
      "getOWLAnonymousIndividual" (signature [] (L AnonymousIndividual.path))
    let m_getOWLObjectAllValuesFrom = get_methodID c
	"getOWLObjectAllValuesFrom" (signature [L ObjectPropertyExpression.path; L ClassExpression.path] (L ObjectAllValuesFrom.path))
    let m_getOWLObjectComplementOf = get_methodID c
	"getOWLObjectComplementOf" (signature [L ClassExpression.path] (L ObjectComplementOf.path))
    let m_getOWLObjectHasValue = get_methodID c
	"getOWLObjectHasValue" (signature [L ObjectPropertyExpression.path; L Individual.path] (L ObjectHasValue.path))
    let m_getOWLObjectIntersectionOf = get_methodID c
	"getOWLObjectIntersectionOf" (signature [L JSet.path] (L ObjectIntersectionOf.path))
    let m_getOWLObjectInverseOf = get_methodID c
	"getOWLObjectInverseOf" (signature [L ObjectPropertyExpression.path] (L ObjectInverseOf.path))
    let m_getOWLObjectOneOf = get_methodID c
	"getOWLObjectOneOf" (signature [L JSet.path] (L ObjectOneOf.path))
    let m_getOWLObjectProperty = get_methodID c
	"getOWLObjectProperty" (signature [L IRI.path] (L ObjectProperty.path))
    let m_getOWLObjectSomeValuesFrom = get_methodID c
	"getOWLObjectSomeValuesFrom" (signature [L ObjectPropertyExpression.path; L ClassExpression.path] (L ObjectSomeValuesFrom.path))
    let m_getOWLObjectUnionOf = get_methodID c
	"getOWLObjectUnionOf" (signature [L JSet.path] (L ObjectUnionOf.path))
    let m_getOWLThing = get_methodID c
	"getOWLThing" (signature [] (L Class.path))
    let m_getOWLNothing = get_methodID c
	"getOWLNothing" (signature [] (L Class.path))
    let m_getOWLTopObjectProperty = get_methodID c
	"getOWLTopObjectProperty" (signature [] (L ObjectProperty.path))
    let m_getRDFSLabel = get_methodID c
	"getRDFSLabel" (signature [] (L AnnotationProperty.path))
    let m_getOWLDeclarationAxiom = get_methodID c
        "getOWLDeclarationAxiom" (signature [L OWLEntity.path] (L Declaration.path))
    let m_getOWLEquivalentClassesAxiom = get_methodID c
        "getOWLEquivalentClassesAxiom" (signature [L ClassExpression.path; L ClassExpression.path] (L EquivalentClasses.path))
    let m_getOWLSubClassOfAxiom = get_methodID c
	"getOWLSubClassOfAxiom" (signature [L ClassExpression.path; L ClassExpression.path] (L SubClassOf.path))
    let m_getOWLSubObjectPropertyOfAxiom = get_methodID c
	"getOWLSubObjectPropertyOfAxiom" (signature [L ObjectPropertyExpression.path; L ObjectPropertyExpression.path] (L SubObjectPropertyOf.path))
    let m_getOWLClassAssertionAxiom = get_methodID c
      "getOWLClassAssertionAxiom" (signature [L ClassExpression.path; L Individual.path] (L ClassAssertion.path))
    let m_getOWLDisjointClassesAxiom = get_methodID c
      "getOWLDisjointClassesAxiom"
      (signature [L JSet.path] (L DisjointClasses.path))
    let m_getOWLInverseObjectPropertiesAxiom = get_methodID c
      "getOWLInverseObjectPropertiesAxiom"
      (signature [L ObjectPropertyExpression.path; L ObjectPropertyExpression.path] (L InverseObjectPropertiesAxiom.path))
    let m_getOWLFunctionalObjectPropertyAxiom = get_methodID c
      "getOWLFunctionalObjectPropertyAxiom"
      (signature [L ObjectPropertyExpression.path] (L FunctionalObjectProperty.path))
    let m_getOWLInverseFunctionalObjectPropertyAxiom = get_methodID c
      "getOWLInverseFunctionalObjectPropertyAxiom"
      (signature [L ObjectPropertyExpression.path] (L InverseFunctionalObjectProperty.path))
    let m_getOWLReflexiveObjectPropertyAxiom = get_methodID c
      "getOWLReflexiveObjectPropertyAxiom"
      (signature [L ObjectPropertyExpression.path] (L ReflexiveObjectProperty.path))
    let m_getOWLIrreflexiveObjectPropertyAxiom = get_methodID c
      "getOWLIrreflexiveObjectPropertyAxiom"
      (signature [L ObjectPropertyExpression.path] (L IrreflexiveObjectProperty.path))
    let m_getOWLSymmetricObjectPropertyAxiom = get_methodID c
      "getOWLSymmetricObjectPropertyAxiom"
      (signature [L ObjectPropertyExpression.path] (L SymmetricObjectProperty.path))
    let m_getOWLAsymmetricObjectPropertyAxiom = get_methodID c
      "getOWLAsymmetricObjectPropertyAxiom"
      (signature [L ObjectPropertyExpression.path] (L AsymmetricObjectProperty.path))
    let m_getOWLTransitiveObjectPropertyAxiom = get_methodID c
      "getOWLTransitiveObjectPropertyAxiom"
      (signature [L ObjectPropertyExpression.path] (L TransitiveObjectProperty.path))

    class obj (this : Jni.obj) =
      object
	inherit JObject.obj this

	method get_named_individual (iri : IRI.obj) = Common.prof "Owlapi.DataFactory.obj#get_named_individual" (fun () ->
	  new NamedIndividual.obj (call_object_method this m_getOWLNamedIndividual [|Jni.Obj iri#jni|]))
	method get_anonymous_individual = Common.prof "Owlapi.DataFactory.obj#get_anonymous_individual" (fun () ->
	  new AnonymousIndividual.obj (call_object_method this m_getOWLAnonymousIndividual [||]))
	method get_class (iri : IRI.obj) = Common.prof "Owlapi.DataFactory.obj#get_class" (fun () ->
	  new Class.obj (call_object_method this m_getOWLClass [|Jni.Obj iri#jni|]))
	method get_object_property (iri : IRI.obj) = Common.prof "Owlapi.DataFactory.obj#get_object_property" (fun () ->
	  new ObjectProperty.obj (call_object_method this m_getOWLObjectProperty [|Jni.Obj iri#jni|]))
	method get_RDFS_label = Common.prof "Owlapi.DataFactory.obj#get_RDFS_label" (fun () ->
	  new AnnotationProperty.obj (call_object_method this m_getRDFSLabel [||]))

	method get_thing = Common.prof "Owlapi.DataFactory.obj#get_thing" (fun () ->
	  new Class.obj (call_object_method this m_getOWLThing [||]))
	method get_nothing = Common.prof "Owlapi.DataFactory.obj#get_nothing" (fun () ->
	  new Class.obj (call_object_method this m_getOWLNothing [||]))
	method get_top_object_property = Common.prof "Owlapi.DataFactory.obj#get_top_object_property" (fun () ->
	  new ObjectProperty.obj (call_object_method this m_getOWLTopObjectProperty [||]))

	method get_object_all_values_from (ope : ObjectPropertyExpression.obj) (ce : ClassExpression.obj) = Common.prof "Owlapi.DataFactory.obj#get_object_all_values_from" (fun () ->
	  new ClassExpression.obj (call_object_method this m_getOWLObjectAllValuesFrom [|Jni.Obj ope#jni; Jni.Obj ce#jni|]))
	method get_object_complement_of (ce : ClassExpression.obj) = Common.prof "Owlapi.DataFactory.obj#get_object_complement_of" (fun () ->
	  new ClassExpression.obj (call_object_method this m_getOWLObjectComplementOf [|Jni.Obj ce#jni|]))
	method get_object_has_value (ope : ObjectPropertyExpression.obj) (ind : Individual.obj) = Common.prof "Owlapi.DataFactory.obj#get_object_has_value" (fun () ->
	  new ClassExpression.obj (call_object_method this m_getOWLObjectHasValue [|Jni.Obj ope#jni; Jni.Obj ind#jni|]))
	method get_object_intersection_of (cel : ClassExpression.obj list) = Common.prof "Owlapi.DataFactory.obj#get_object_intersection_of" (fun () ->
	  let set = JTreeSet.from_list (new ClassExpression.obj) cel in
	  new ClassExpression.obj (call_object_method this m_getOWLObjectIntersectionOf [|Jni.Obj set#jni|]))
	method get_object_one_of (indl : Individual.obj list) = Common.prof "Owlapi.DataFactory.obj#get_object_one_of" (fun () ->
	  let set = JTreeSet.from_list (new Individual.obj) indl in
	  new ClassExpression.obj (call_object_method this m_getOWLObjectOneOf [|Jni.Obj set#jni|]))
	method get_object_some_values_from (ope : ObjectPropertyExpression.obj) (ce : ClassExpression.obj) = Common.prof "Owlapi.DataFactory.obj#get_object_some_values_from" (fun () ->
	  new ClassExpression.obj (call_object_method this m_getOWLObjectSomeValuesFrom [|Jni.Obj ope#jni; Jni.Obj ce#jni|]))
	method get_object_union_of (cel : ClassExpression.obj list) = Common.prof "Owlapi.DataFactory.obj#get_object_union_of" (fun () ->
	  let set = JTreeSet.from_list (new ClassExpression.obj) cel in
	  new ClassExpression.obj (call_object_method this m_getOWLObjectUnionOf [|Jni.Obj set#jni|]))

	method get_object_inverse_of (ope : ObjectPropertyExpression.obj) = Common.prof "Owlapi.DataFactory.obj#get_object_inverse_of" (fun () ->
	  new ObjectPropertyExpression.obj (call_object_method this m_getOWLObjectInverseOf [|Jni.Obj ope#jni|]))

	method get_declaration (e : OWLEntity.obj) = Common.prof "Owlapi.DataFactory.obj#get_declaration" (fun () ->
	  new Axiom.obj (call_object_method this m_getOWLDeclarationAxiom [|Jni.Obj e#jni|]))
	method get_subclass_of (ce1 : ClassExpression.obj) (ce2 : ClassExpression.obj) = Common.prof "Owlapi.DataFactory.obj#get_subclass_of" (fun () ->
	  new Axiom.obj (call_object_method this m_getOWLSubClassOfAxiom [|Jni.Obj ce1#jni; Jni.Obj ce2#jni|]))
	method get_equivalent_classes (ce1 : ClassExpression.obj) (ce2 : ClassExpression.obj) = Common.prof "Owlapi.DataFactory.obj#get_equivalent_classes" (fun () ->
	  new Axiom.obj (call_object_method this m_getOWLEquivalentClassesAxiom [|Jni.Obj ce1#jni; Jni.Obj ce2#jni|]))
	method get_class_assertion (ce : ClassExpression.obj) (ind : Individual.obj) = Common.prof "Owlapi.DataFactory.obj#get_class_assertion" (fun () ->
	  new Axiom.obj (call_object_method this m_getOWLClassAssertionAxiom [|Jni.Obj ce#jni; Jni.Obj ind#jni|]))
	method get_disjoint_classes (cel : ClassExpression.obj list) = Common.prof "Owlapi.DataFactory.obj#get_disjoint_classes" (fun () ->
	  let set = JTreeSet.from_list (new ClassExpression.obj) cel in
	  new Axiom.obj (call_object_method this m_getOWLDisjointClassesAxiom [|Jni.Obj set#jni|]))
	method get_subobjectproperty_of (pe1 : ObjectPropertyExpression.obj) (pe2 : ObjectPropertyExpression.obj) = Common.prof "Owlapi.DataFactory.obj#get_subobjectproperty_of" (fun () ->
	  new Axiom.obj (call_object_method this m_getOWLSubObjectPropertyOfAxiom [|Jni.Obj pe1#jni; Jni.Obj pe2#jni|]))
	method get_functional_object_property (pe : ObjectPropertyExpression.obj) = Common.prof "Owlapi.Datafactory.obj#get_functional_object_property" (fun () ->
	  new Axiom.obj (call_object_method this m_getOWLFunctionalObjectPropertyAxiom [|Jni.Obj pe#jni|]))
	method get_inverse_object_properties (pe1 : ObjectPropertyExpression.obj) (pe2 : ObjectPropertyExpression.obj) = Common.prof "Owlapi.DataFactory.obj#get_inverse_object_properties" (fun () ->
	  new Axiom.obj (call_object_method this m_getOWLInverseObjectPropertiesAxiom [|Jni.Obj pe1#jni; Jni.Obj pe2#jni|]))
	method get_inverse_functional_object_property (pe : ObjectPropertyExpression.obj) = Common.prof "Owlapi.Datafactory.obj#get_inverse_functional_object_property" (fun () ->
	  new Axiom.obj (call_object_method this m_getOWLInverseFunctionalObjectPropertyAxiom [|Jni.Obj pe#jni|]))
	method get_reflexive_object_property (pe : ObjectPropertyExpression.obj) = Common.prof "Owlapi.Datafactory.obj#get_reflexive_object_property" (fun () ->
	  new Axiom.obj (call_object_method this m_getOWLReflexiveObjectPropertyAxiom [|Jni.Obj pe#jni|]))
	method get_irreflexive_object_property (pe : ObjectPropertyExpression.obj) = Common.prof "Owlapi.Datafactory.obj#get_irreflexive_object_property" (fun () ->
	  new Axiom.obj (call_object_method this m_getOWLIrreflexiveObjectPropertyAxiom [|Jni.Obj pe#jni|]))
	method get_symmetric_object_property (pe : ObjectPropertyExpression.obj) = Common.prof "Owlapi.Datafactory.obj#get_symmetric_object_property" (fun () ->
	  new Axiom.obj (call_object_method this m_getOWLSymmetricObjectPropertyAxiom [|Jni.Obj pe#jni|]))
	method get_asymmetric_object_property (pe : ObjectPropertyExpression.obj) = Common.prof "Owlapi.Datafactory.obj#get_asymmetric_object_property" (fun () ->
	  new Axiom.obj (call_object_method this m_getOWLAsymmetricObjectPropertyAxiom [|Jni.Obj pe#jni|]))
	method get_transitive_object_property (pe : ObjectPropertyExpression.obj) = Common.prof "Owlapi.Datafactory.obj#get_transitive_object_property" (fun () ->
	  new Axiom.obj (call_object_method this m_getOWLTransitiveObjectPropertyAxiom [|Jni.Obj pe#jni|]))
      end
  end

module OntologyChange =
  struct
    let path = "org/semanticweb/owlapi/model/OWLOntologyChange"
  end

module Ontology =
  struct
    let path = "org/semanticweb/owlapi/model/OWLOntology"
    let c = find_class path
    let m_toString = get_methodID c
	"toString" (signature [] (L JString.path))
    let m_getIndividualsInSignature = get_methodID c
	"getIndividualsInSignature" (signature [] (L JSet.path))
    let m_getClassesInSignature = get_methodID c
	"getClassesInSignature" (signature [] (L JSet.path))
    let m_getObjectPropertiesInSignature = get_methodID c
	"getObjectPropertiesInSignature" (signature [] (L JSet.path))

    class obj (this : Jni.obj) =
      object
	inherit JObject.obj this

	method get_individuals = Common.prof "Owlapi.Ontology.obj#get_individuals" (fun () ->
	  new JSet.obj (new NamedIndividual.obj) (call_object_method this m_getIndividualsInSignature [||]))
	method get_classes = Common.prof "Owlapi.Ontology.obj#get_classes" (fun () ->
	  new JSet.obj (new Class.obj) (call_object_method this m_getClassesInSignature [||]))
	method get_object_properties = Common.prof "Owlapi.Ontology.obj#get_object_properties" (fun () ->
	  new JSet.obj (new ObjectProperty.obj) (call_object_method this m_getObjectPropertiesInSignature [||]))
      end
  end

module OntologyManager =
  struct
    let path = "org/semanticweb/owlapi/model/OWLOntologyManager"
    let c = find_class path
    let m_getOWLDataFactory = get_methodID c
	"getOWLDataFactory" (signature [] (L DataFactory.path))
    let m_createOntology = get_methodID c
	"createOntology" (signature [] (L Ontology.path))
    let m_createOntology_IRI = get_methodID c
	"createOntology" (signature [L IRI.path] (L Ontology.path))
    let m_loadOntologyFromOntologyDocument_IRI = get_methodID c
	"loadOntologyFromOntologyDocument" (signature [L IRI.path] (L Ontology.path))
    let m_loadOntologyFromOntologyDocument_File = get_methodID c
	"loadOntologyFromOntologyDocument" (signature [L JFile.path] (L Ontology.path))
    let m_saveOntology = get_methodID c
	"saveOntology" (signature [L Ontology.path] V)
    let m_saveOntology_IRI = get_methodID c
	"saveOntology" (signature [L Ontology.path; L IRI.path] V)
    let m_saveOntology_OutputStream = get_methodID c
	"saveOntology" (signature [L Ontology.path; L JOutputStream.path] V)
    let m_addAxiom = get_methodID c
	"addAxiom" (signature [L Ontology.path; L Axiom.path] (L JList.path))
    let m_removeAxiom = get_methodID c
      "removeAxiom" (signature [L Ontology.path; L Axiom.path] (L JList.path))
    let m_applyChanges = get_methodID c
      "applyChanges" (signature [L JList.path] (L JList.path))

    class obj (this : Jni.obj) =
      object
	inherit JObject.obj this

	method get_data_factory = Common.prof "Owlapi.OntologyManager.obj#get_data_factory" (fun () ->
	  new DataFactory.obj (call_object_method this m_getOWLDataFactory [||]))

	method create_ontology = Common.prof "Owlapi.OntologyManager.obj#create_ontology" (fun () ->
	  new Ontology.obj (call_object_method this m_createOntology [||]))

	method create_ontology_with_iri (iri : IRI.obj) = Common.prof "Owlapi.OntologyManager.obj#create_ontology_with_iri" (fun () ->
	  new Ontology.obj (call_object_method this m_createOntology [|Jni.Obj iri#jni|]))

	method load_ontology_from_iri (iri : IRI.obj) = Common.prof "Owlapi.OntologyManager.obj#load_ontology_from_iri" (fun () ->
	  new Ontology.obj (call_object_method this m_loadOntologyFromOntologyDocument_IRI [|Jni.Obj iri#jni|]))

	method load_ontology_from_file (file : JFile.obj) = Common.prof "Owlapi.OntologyManager.obj#load_ontology_from_file" (fun () ->
	  new Ontology.obj (call_object_method this m_loadOntologyFromOntologyDocument_File [|Jni.Obj file#jni|]))

	method save_ontology (ont : Ontology.obj) = Common.prof "Owlapi.OntologyManager.obj#save_ontology" (fun () ->
	  call_void_method this m_saveOntology [|Jni.Obj ont#jni|])

	method save_ontology_to_iri (ont : Ontology.obj) (iri : IRI.obj) = Common.prof "Owlapi.OntologyManager.obj#save_ontology_to_iri" (fun () ->
	  call_void_method this m_saveOntology [|Jni.Obj ont#jni; Jni.Obj iri#jni|])

	method save_ontology_to_output_stream (ont : Ontology.obj) (out : JOutputStream.obj) = Common.prof "Owlapi.OntologyManager.obj#save_ontology_to_output_stream" (fun () ->
	  call_void_method this m_saveOntology [|Jni.Obj ont#jni; Jni.Obj out#jni|])

	method add_axiom (ont : Ontology.obj) (ax : Axiom.obj) : unit = Common.prof "Owlapi.OntologyManager.obj#add_axiom" (fun () ->
	  ignore (call_object_method this m_addAxiom [|Jni.Obj ont#jni; Jni.Obj ax#jni|]))

	method remove_axiom (ont : Ontology.obj) (ax : Axiom.obj) : unit = Common.prof "Owlapi.OntologyManager.obj#remove_axiom" (fun () ->
	  ignore (call_object_method this m_removeAxiom [|Jni.Obj ont#jni; Jni.Obj ax#jni|]))
      end
  end

module Node =
  struct
    let path = "org/semanticweb/owlapi/reasoner/Node"
    let c = find_class path
    let m_getRepresentativeElement = get_methodID c
	"getRepresentativeElement" (signature [] (L OWLObject.path))
    let m_isBottomNode = get_methodID c
	"isBottomNode" (signature [] Z)

    class ['e] obj (make_e : Jni.obj -> 'e) (this : Jni.obj) =
      object
	inherit JObject.obj this
	inherit ['e] Iterable.obj make_e this

	method get_representative_element = Common.prof "Owlapi.Node.obj#get_representative_element" (fun () ->
	  make_e (call_object_method this m_getRepresentativeElement [||]))
	method is_bottom_node = Common.prof "Owlapi.Node.obj#is_bottom_node" (fun () ->
	  call_boolean_method this m_isBottomNode [||])
      end
  end

module NodeSet =
  struct
    let path = "org/semanticweb/owlapi/reasoner/NodeSet"
    let c = find_class path

    class ['e] obj (make_e : Jni.obj -> 'e) (this : Jni.obj) =
      object
	inherit JObject.obj this
	inherit ['e Node.obj] Iterable.obj (new Node.obj make_e) this
      end
  end

module Reasoner =
  struct
    let path = "org/semanticweb/owlapi/reasoner/OWLReasoner"
    let c = find_class path
    let m_flush = get_methodID c
	"flush" (signature [] V)
    let m_isConsistent = get_methodID c
	"isConsistent" (signature [] Z)
    let m_isEntailed = get_methodID c
	"isEntailed" (signature [L Axiom.path] Z)
    let m_isSatisfiable = get_methodID c
	"isSatisfiable" (signature [L ClassExpression.path] Z)
    let m_getTopClassNode = get_methodID c
	"getTopClassNode" (signature [] (L Node.path))
    let m_getBottomClassNode = get_methodID c
	"getBottomClassNode" (signature [] (L Node.path))
    let m_getSubClasses = get_methodID c
	"getSubClasses" (signature [L ClassExpression.path; Z] (L NodeSet.path))
    let m_getInstances = get_methodID c
	"getInstances" (signature [L ClassExpression.path; Z] (L NodeSet.path))
    let m_getTopObjectPropertyNode = get_methodID c
	"getTopObjectPropertyNode" (signature [] (L Node.path))
    let m_getSubObjectProperties = get_methodID c
	"getSubObjectProperties" (signature [L ObjectPropertyExpression.path; Z] (L NodeSet.path))
    let m_getInverseObjectProperties = get_methodID c
	"getInverseObjectProperties" (signature [L ObjectPropertyExpression.path] (L Node.path))
    let m_getObjectPropertyValues = get_methodID c
	"getObjectPropertyValues" (signature [L NamedIndividual.path; L ObjectPropertyExpression.path] (L NodeSet.path))

    class obj (this : Jni.obj) =
      object
	inherit JObject.obj this

	method flush = Common.prof "Owlapi.Reasoner.obj#flush" (fun () ->
	  call_void_method this m_flush [||])

	method is_consistent = Common.prof "Owlapi.Reasoner.obj#is_consistent" (fun () ->
	  call_boolean_method this m_isConsistent [||])
	method is_entailed (ax : Axiom.obj) = Common.prof "Owlapi.Reasoner.obj#is_entailed" (fun () ->
	  call_boolean_method this m_isEntailed [|Jni.Obj ax#jni|])
	method is_satisfiable (ce : ClassExpression.obj) = Common.prof "Owlapi.Reasoner.obj#is_satisfiable" (fun () ->
	  call_boolean_method this m_isSatisfiable [|Jni.Obj ce#jni|])

	method get_top_class_node = Common.prof "Owlapi.Reasoner.obj#get_top_class_node" (fun () ->
	  new Node.obj (new Class.obj) (call_object_method this m_getTopClassNode [||]))
	method get_bottom_class_node = Common.prof "Owlapi.Reasoner.obj#get_bottom_class_node" (fun () ->
	  new Node.obj (new Class.obj) (call_object_method this m_getBottomClassNode [||]))
	method get_subclasses (ce : ClassExpression.obj) (direct : bool) = Common.prof "Owlapi.Reasoner.obj#get_subclasses" (fun () ->
	  new NodeSet.obj (new Class.obj) (call_object_method this m_getSubClasses [|Jni.Obj ce#jni; Jni.Boolean direct|]))
	method get_instances (ce : ClassExpression.obj) (direct : bool) = Common.prof "Owlapi.Reasoner.obj#get_instances" (fun () ->
	  new NodeSet.obj (new Individual.obj) (call_object_method this m_getInstances [|Jni.Obj ce#jni; Jni.Boolean direct|]))

	method get_top_object_property_node = Common.prof "Owlapi.Reasoner.obj#get_top_object_property_node" (fun () ->
	  new Node.obj (new ObjectPropertyExpression.obj) (call_object_method this m_getTopObjectPropertyNode [||]))
	method get_object_subproperties (pe : ObjectPropertyExpression.obj) (direct : bool) = Common.prof "Owlapi.Reasoner.obj#get_object_subproperties" (fun () ->
	  new NodeSet.obj (new ObjectPropertyExpression.obj) (call_object_method this m_getSubObjectProperties [|Jni.Obj pe#jni; Jni.Boolean direct|]))
	method get_inverse_object_properties (pe : ObjectPropertyExpression.obj) = Common.prof "Owlapi.Reasoner.obj#get_inverse_object_properties" (fun () ->
	  new Node.obj (new ObjectPropertyExpression.obj) (call_object_method this m_getInverseObjectProperties [|Jni.Obj pe#jni|]))

	method get_object_property_values (ind : NamedIndividual.obj) (pe : ObjectPropertyExpression.obj) = Common.prof "Owlapi.Reasoner.obj#get_object_property_values" (fun () ->
	  new NodeSet.obj (new NamedIndividual.obj) (call_object_method this m_getObjectPropertyValues [|Jni.Obj ind#jni; Jni.Obj pe#jni|]))
      end
  end

module ReasonerFactory =
  struct
    let path = "org/semanticweb/HermiT/Reasoner$ReasonerFactory"

(* does not work with Pellet *)
(*    let path = "org/mindswap/pellet/owlapi/PelletReasonerFactory"*)
(*    let path = "com/clarkparsia/pellet/owlapiv3/PelletReasonerFactory"*)

    let c = find_class path
    let init = get_methodID c
	"<init>" (signature [] V)
    let m_createReasoner = get_methodID c
	"createReasoner" (signature [L Ontology.path] (L Reasoner.path))

    class obj (this : Jni.obj) =
      object
	inherit JObject.obj this

	method create_reasoner (ont : Ontology.obj) = Common.prof "Owlapi.ReasonerFactory.obj#create_reasoner" (fun () ->
	  new Reasoner.obj (call_object_method this m_createReasoner [|Jni.Obj ont#jni|]))
      end

    let create () : obj = Common.prof "Owlapi.ReasonerFactory.create" (fun () ->
      let this = Jni.alloc_object c in
      call_nonvirtual_void_method this c init [||];
      new obj this)
  end

module Manager =
  struct
    let path = "org/semanticweb/owlapi/apibinding/OWLManager"
    let c = find_class path
    let m_createOWLOntologyManager = get_static_methodID c
	"createOWLOntologyManager" (signature [] (L OntologyManager.path))
    let m_getOWLDataFactory = get_static_methodID c
	"getOWLDataFactory" (signature [] (L DataFactory.path))

    let create_OntologyManager () = Common.prof "Owlapi.Manager.create_OntologyManager" (fun () ->
      new OntologyManager.obj (call_static_object_method c m_createOWLOntologyManager [||]))

    let get_DataFactory () = Common.prof "Owlapi.Manager.get_DataFactory" (fun () ->
      new DataFactory.obj (call_static_object_method c m_getOWLDataFactory [||]))
  end

module EntityRenamer =
struct
  let path = "org/semanticweb/owlapi/util/OWLEntityRenamer"
  let c = find_class path
  let init = get_methodID c
    "<init>" (signature [L OntologyManager.path; L JSet.path (* <Ontology> *)] V)
  let m_changeIRI = get_methodID c
    "changeIRI" (signature [L IRI.path; L IRI.path] (L JList.path (* <OntologyChange> *)))
    
  class obj (ont_manager : OntologyManager.obj) (this : Jni.obj) =
  object
    inherit JObject.obj this

    method change_iri (iri : IRI.obj) (new_iri : IRI.obj) = Common.prof "Owlapi.EntityRenamer#changeIRI" (fun () ->
      let jni_changes = call_object_method this m_changeIRI [|Jni.Obj iri#jni; Jni.Obj new_iri#jni|] in
      ignore (call_object_method ont_manager#jni OntologyManager.m_applyChanges [|Jni.Obj jni_changes|]))
  end

  let create (ont_manager : OntologyManager.obj) (ont_set : Ontology.obj JSet.obj) : obj = Common.prof "EntityRenamer.create" (fun () ->
    let this = Jni.alloc_object c in
    call_nonvirtual_void_method this c init [|Jni.Obj ont_manager#jni; Jni.Obj ont_set#jni|];
    new obj ont_manager this)
end
    
let _ = print_endline "The OCaml wrapping over OWL API has completed."
