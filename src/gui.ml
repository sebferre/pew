(** Graphical interface for Openexp (adapted from Sewelis). *)

module Expr = Expression
module Main = Ontology

open Format
open StdLabels
open GdkKeysyms
open Gobject.Data

let uri_pizza = "http://www.co-ode.org/ontologies/pizza/pizza.owl"
let path_pizza = "/local/ferre/data/ontologies/mypizza.owl"

(* command line arguments *)

let _ =
  Gc.set { (Gc.get())
	 with
	   Gc.major_heap_increment = 512*1024;
	   Gc.space_overhead = 50;
	   Gc.max_overhead = 50;
           Gc.stack_limit = 8 * 1024 * 1024 }

(* utilities *)

let do_while_idle : (unit -> unit) Queue.t -> Glib.Idle.id =
  fun q ->
    Glib.Idle.add (fun () ->
      try Queue.take q () ; true
      with Queue.Empty -> false)

let utf s =
  if Glib.Utf8.validate s
  then s
  else try Glib.Convert.locale_to_utf8 s with _ -> s

let loc s =
  try Glib.Convert.locale_from_utf8 s with _ -> s

let rec create_directory dirname =
  let parent = Filename.dirname dirname in
  if not (Sys.file_exists parent) then
    create_directory parent;
  Unix.mkdir dirname 0o751

let create_file filename =
  let dirname = Filename.dirname filename in
  if not (Sys.file_exists dirname) then
    create_directory dirname;
  let ch = open_out filename in (* creating the file *)
  close_out ch

let wget url path =
  let code = Sys.command ("wget -q -O " ^ path ^ " \"" ^ url ^ "\"") in
  if code <> 0 then invalid_arg ("Gui.path_of_url: invalid url: " ^ url)

let path_of_url =
  let file_prefix = "file://" in
  let k = String.length file_prefix in
  fun url ->
    let n = String.length url in
    if k < n && String.sub url 0 k = file_prefix
    then String.sub url k (n-k)
    else
      let path = Filename.temp_file "url" "" in
      wget url path;
      path


let dir_sep =
  match Sys.os_type with
  | "Unix" -> "/"
  | "Cygwin" -> "/"
  | "Win32" -> "\\"
  | _ -> "/"

let last_dir = ref (Sys.getcwd () ^ dir_sep)

let awaken f final =
  ignore (f ());
  final ()

(* graphical utilities *)

let pixbuf_of_picture =
  let ht = Hashtbl.create 100 in
  fun ~maxside url ->
    try Hashtbl.find ht (url,maxside)
    with Not_found ->
      let filename = path_of_url url in
      let pixbuf = GdkPixbuf.from_file_at_size filename ~width:maxside ~height:maxside in
      Hashtbl.add ht (filename,maxside) pixbuf;
      pixbuf

let error_message msg =
  let md = GWindow.message_dialog
      ~message:(utf msg)
      ~message_type:`ERROR
      ~buttons:GWindow.Buttons.ok
      ~modal:true () in
  md#run ();
  md#destroy ()

let info_message ?title ?icon msg =
  let md = GWindow.message_dialog
      ~message:(utf msg)
      ~message_type:`INFO
      ?title
      ?icon
      ~buttons:GWindow.Buttons.ok
      ~modal:true () in
  md#run ();
  md#destroy ()

let question msg =
  let md = GWindow.message_dialog
      ~message:(utf msg)
      ~message_type:`QUESTION
      ~buttons:GWindow.Buttons.yes_no
      ~modal:true () in
  let res = md#run () in
  md#destroy ();
  res = `YES

let confirm_overwrite filename =
  not (Sys.file_exists filename) || question "This file already exists. Are you sure you want to overwrite it ?"

let file_dialog ~title ?filename ?filter ~callback () =
  let sel = GWindow.file_selection ~title ~modal:true ?filename () in
  sel#cancel_button#connect#clicked ~callback:sel#destroy;
  sel#ok_button#connect#clicked
    ~callback:(fun () ->
      let name = loc sel#filename in
      sel#destroy ();
      try callback name
      with exn -> error_message (Printexc.to_string exn));
  sel#show ();
  ( match filter with
  | Some s -> sel#complete ~filter:s
  | None -> ())


let syntax_error_logic msg =
  error_message (if msg="" then "Syntax error" else msg)

let date_dialog ~title ~(callback : year:int -> month:int -> day:int -> unit) =
  let w = GWindow.dialog
      ~destroy_with_parent:true
      ~modal:true
      ~title
      () in
  let vbox = w#vbox in
  let cal = GMisc.calendar
      ~options:[`SHOW_DAY_NAMES; `SHOW_HEADING; `SHOW_WEEK_NUMBERS]
      ~packing:vbox#pack
      () in
  let action_area = w#action_area in
  let button_cancel = GButton.button ~stock:`CANCEL ~packing:action_area#add () in
  let button_ok = GButton.button ~stock:`OK ~packing:action_area#add () in
  let action () =
    w#destroy ();
    callback ~year:cal#year ~month:(1+cal#month) ~day:cal#day in
  cal#connect#day_selected_double_click ~callback:action;
  button_cancel#connect#clicked ~callback:w#destroy;
  button_ok#connect#clicked ~callback:action;
  w#show ()

let dateTime_dialog ~title
    ~(callback : year:int -> month:int -> day:int -> hour:int -> min:int -> sec:int -> unit) =
  let w = GWindow.dialog
      ~destroy_with_parent:true
      ~modal:true
      ~title
      () in
  let vbox = w#vbox in
  let cal = GMisc.calendar
      ~options:[`SHOW_DAY_NAMES; `SHOW_HEADING; `SHOW_WEEK_NUMBERS]
      ~packing:vbox#pack
      () in
  let hbox_time = GPack.hbox ~spacing:8 ~packing:vbox#pack () in
  let label_time = GMisc.label ~text:"Time " ~packing:hbox_time#pack () in
  let tm = Unix.localtime (Unix.gettimeofday ()) in
  let spin_hour = GEdit.spin_button
      ~adjustment:(GData.adjustment ~upper:23. ())
      ~digits:0
      ~value:(float_of_int tm.Unix.tm_hour)
      ~packing:hbox_time#pack
      () in
  let spin_min = GEdit.spin_button
      ~adjustment:(GData.adjustment ~upper:59. ())
      ~digits:0
      ~value:(float_of_int tm.Unix.tm_min)
      ~packing:hbox_time#pack
      () in
  let spin_sec = GEdit.spin_button
      ~adjustment:(GData.adjustment ~upper:59. ())
      ~digits:0
      ~value:(float_of_int tm.Unix.tm_sec)
      ~packing:hbox_time#pack
      () in
  let action_area = w#action_area in
  let button_cancel = GButton.button ~stock:`CANCEL ~packing:action_area#add () in
  let button_ok = GButton.button ~stock:`OK ~packing:action_area#add () in
  let action () =
    w#destroy ();
    callback
      ~year:cal#year ~month:(1+cal#month) ~day:cal#day
      ~hour:spin_hour#value_as_int ~min:spin_min#value_as_int ~sec:spin_sec#value_as_int in
  button_cancel#connect#clicked ~callback:w#destroy;
  button_ok#connect#clicked ~callback:action;
  w#show ()

let parse_dialog ~title ?text ~(parse:string -> 'a) ~(callback : 'a -> unit) =
  let w = GWindow.dialog
      ~destroy_with_parent:true
      ~modal:true
      ~title
      () in
  let vbox = w#vbox in
  let entry = GEdit.entry ?text ~width_chars:80 ~packing:vbox#add () in
  let action_area = w#action_area in
  let button_cancel = GButton.button ~stock:`CANCEL ~packing:action_area#add () in
  let button_ok = GButton.button ~stock:`OK ~packing:action_area#add () in
  let action () =
    try
      let res = parse entry#text in
      w#destroy ();
      callback res
    with
    | Stream.Error msg
    | Failure msg -> syntax_error_logic msg
    | e -> error_message (Printexc.to_string e) in
  entry#connect#activate ~callback:action;
  button_cancel#connect#clicked ~callback:w#destroy;
  button_ok#connect#clicked ~callback:action;
  w#show ()

let fields_dialog ~title ~(fields: (string * 'a) list) ~(callback : 'b list -> unit) =
  (* takes a list of (label, field spec) and a callback on entered values *)
  let w = GWindow.dialog
      ~destroy_with_parent:true
      ~modal:true
      ~title
      () in
  let vbox = w#vbox in
  let widgets =
    List.map
      (fun (label, spec) ->
	ignore (GMisc.label ~text:label ~xalign:0. ~packing:vbox#add ());
	match spec with
	| `String text -> `String (GEdit.entry ~text ~width_chars:60 ~packing:vbox#add ())
	| `Integer v -> `Integer (GEdit.spin_button ~adjustment:(GData.adjustment ~page_size:10. ~value:(float_of_int v) ()) ~packing:vbox#add ())
	| `Enum (active,l) -> `Enum (l, GEdit.combo_box_text ~strings:(List.map fst l) ~active ~packing:vbox#add ()))
      fields in
  let action_area = w#action_area in
  let button_cancel = GButton.button ~stock:`CANCEL ~packing:action_area#add () in
  let button_ok = GButton.button ~stock:`OK ~packing:action_area#add () in
  let action () =
    try
      let res =
	List.map (function
	  | `String entry -> `String entry#text
	  | `Integer spin_button -> `Integer spin_button#value_as_int
	  | `Enum (l, combo_box_text) -> `Enum (List.assoc (Option.find (GEdit.text_combo_get_active combo_box_text)) l))
	  widgets in
      callback res;
      w#destroy ()
    with
    | Stream.Error msg
    | Failure msg -> syntax_error_logic msg
    | e -> error_message (Printexc.to_string e) in
  List.iter (function
    | `String entry -> ignore (entry#connect#activate ~callback:action)
    | `Integer spin_button -> ignore (spin_button#connect#activate ~callback:action)
    | _ -> ())
    widgets;
  button_cancel#connect#clicked ~callback:w#destroy;
  button_ok#connect#clicked ~callback:action;
  w#show ()

let mint_uri store label uri =
  if uri = ""
  then store#base ^ label (* TODO: escape non-URI characters *)
  else Uri.resolve store#base store#xmlns uri

let fragment_of_iri base iri =
  let l_base = String.length base in
  let l_iri = String.length iri in
  if l_base < l_iri && String.sub iri 0 l_base = base
  then Some (String.sub iri l_base (l_iri - l_base))
  else None
  
let iri_dialog store ~title ?(fragment = "") ~(callback : Expr.iri -> unit) () =
  fields_dialog ~title
    ~fields:[("Fragment", `String fragment)]
    ~callback:(function
      | [`String fragment] ->
	  let iri = store#base ^ fragment in
	  callback iri
      | _ -> assert false)

let uri_dialog store ~title ?(label = "") ?(uri = "") ~(callback : string -> unit) () =
  fields_dialog ~title
    ~fields:[("Label", `String label); ("URI (optional)", `String uri)]
    ~callback:(function
      | [`String label; `String uri] ->
	  let uri = mint_uri store label uri in
	  if label <> "" then store#set_label uri label;
	  callback uri
      | _ -> assert false)

let class_dialog store ~title ?(label = "") ?(uri = "") ~(callback : Uri.t -> unit) () =
  fields_dialog ~title
    ~fields:[("Label", `String label); ("URI (optional)", `String uri)]
    ~callback:(function
      | [`String label; `String uri] ->
	  let uri = mint_uri store label uri in
	  if label <> "" then store#set_label uri label;
	  store#add_type (Rdf.URI uri) Rdfs.uri_Class;
	  callback uri
      | _ -> assert false)

let property_dialog store ~title ?(label = "") ?(inverse_label = "") ?(uri = "") ~(callback : Uri.t -> unit) () =
  fields_dialog ~title
    ~fields:[("Label", `String label);
	     ("Inverse label (optional)", `String inverse_label);
	     ("URI (optional)", `String uri)]
    ~callback:(function
      | [`String label; `String inverse_label; `String uri] ->
	  let uri = mint_uri store label uri in
	  if label <> "" then store#set_label uri label;
	  if inverse_label <> "" then store#set_inverseLabel uri inverse_label;
	  store#add_type (Rdf.URI uri) Rdf.uri_Property;
	  (* should add property properties: transitive, symmetric, ... *)
	  callback uri
      | _ -> assert false)

let paned_set_ratio paned n d =
  paned#set_position (((d-n) * paned#min_position + n * paned#max_position) / d) (* position aux n/d *)

(* Application specific code *)

let filter_nt = "*.nt"
let filter_owl = "*.owl"
let filter_jpg = "*.jpg"

let str_white = "white"
let str_black = "black"
let str_lightgrey = "lightgrey"
let str_darkgrey = "darkgrey"
let str_darkred = "darkred"
let str_darkorange = "darkorange"
let str_darkyellow = "darkyellow"
let str_darkgreen = "darkgreen"
let str_darkcyan = "darkcyan"
let str_darkblue = "darkblue"
let str_darkviolet = "darkviolet"
let str_default_color = "lightgrey"


let str_color_of_rgb (r,g,b) = Printf.sprintf "#%02x%02x%02x" r g b

let str_color_necessary = str_color_of_rgb (200,255,200) (*str_color_of_rgb (200,200,255)*)
let str_color_satisfiable = str_color_of_rgb (255,255,200) (*str_color_of_rgb (200,255,200)*)
let str_color_unsatisfiable = str_color_of_rgb (255,200,200)

let str_color_necessary_prelight = str_color_of_rgb (210,255,210) (*str_color_of_rgb (210,210,255)*)
let str_color_satisfiable_prelight = str_color_of_rgb (255,255,210) (*str_color_of_rgb (210,255,210)*)
let str_color_unsatisfiable_prelight = str_color_of_rgb (255,210,210)

let str_color_modifier = str_darkred
let str_color_entity = str_darkblue
let str_color_literal = str_darkgreen
let str_color_class = str_darkorange
let str_color_property = str_darkviolet
let str_color_functor = str_darkcyan


let modify_bg widget (str_color : string) (str_color_prelight : string) =
  let new_style = widget#misc#style#copy in
  new_style#set_bg [(`NORMAL, `NAME str_color); (`PRELIGHT, `NAME str_color_prelight)];
  widget#misc#set_style new_style


type incr = {
    incr_cls : Expr.cls; (* the increment logical expression itself *)
    incr_str : string;
    incr_markup : string;
    incr_weight : int;
    incr_scale : float; (* scale representing support/query *)
    incr_color : string; (* grey-level equivalent to scale *)
    incr_bg_color : string;
    incr_iri_opt : Expr.iri option; (* the URI that the increments possibly contains *)
  }

let compare i1 i2 = Expr.compare i1.incr_cls i2.incr_cls


type feature_kind = Literal | Entity | Class | Property | InverseProperty | Other

let rec kind_of_class x =
  match x with
  | Expr.Individual _ -> Entity
  | Expr.Class _ -> Class
  | Expr.ObjectExists (Expr.Inverse _, _) -> InverseProperty
  | Expr.ObjectExists (_, _) -> Property
(*  | Expr.Compl x1 -> kind_of_class x1 *)
  | _ -> Other

let iri_of_class x =
  match x with
  | Expr.Individual iri -> Some iri
  | Expr.Class iri -> Some iri
  | Expr.ObjectExists (_, Expr.Individual iri) -> Some iri
  | Expr.ObjectExists (Expr.ObjectProperty iri, Expr.Thing) -> Some iri
  | Expr.ObjectExists (Expr.Inverse (Expr.ObjectProperty iri), Expr.Thing) -> Some iri
  | _ -> None

let prop_of_class = function
  | Expr.ObjectExists (prop, _) -> Some prop
  | _ -> None
    
let name_of_concept x =
  match x with
  | Expr.Individual iri -> Some iri
  | Expr.ObjectExists (_, Expr.Individual iri) -> Some iri
  | _ -> None

let default_links_page_size = 100

class state_view =
  object (self)
    val mutable expanded : bool = false
    val mutable links_opt : incr list option = None
    val mutable links_sort_fun : incr -> incr -> int = compare
    val mutable links_page_start : int = 1 (* first page *)
    val mutable links_page_size : int = default_links_page_size (*  use max_int for no limit *)
	
    method expanded = expanded
    method links_opt = links_opt
    method links_sort_fun = links_sort_fun
    method links_page_start = links_page_start
    method links_page_size = links_page_size

    method set_expanded b = expanded <- b

    method set_links_sort_fun f = links_sort_fun <- f
    method set_links_page_start s = links_page_start <- s
    method set_links_page_size s = links_page_size <- s

    method set_links_opt l = links_opt <- l

    method copy =
      {< links_opt = None;
	 links_page_start = 1>}
  end

type id_view = ViewClass of Expr.cls | ViewExtent

class state store (foc_q : Expr.focus_cls) = 
  object (self)
    val mutable focus_query : Expr.focus_cls = foc_q
    val mutable place : Main.place = store#place foc_q
    val mutable views : (id_view,state_view) Hashtbl.t = Hashtbl.create 13

    method focus_query = focus_query
    method place = place
    method views = views

    method new_view = new state_view

    method copy_view (sv : state_view) = sv#copy

    method get_view v =
      try Hashtbl.find views v
      with Not_found ->
	let sv = new state_view in
	Hashtbl.add views v sv;
	sv

    method replace_view : id_view -> id_view -> unit =
      fun v v' ->
	try
	  Hashtbl.replace views v' (Hashtbl.find views v);
	  Hashtbl.remove views v
	with _ -> ()

    method fold_views : 'a.(id_view -> state_view -> 'a -> 'a) -> 'a -> 'a = fun f e -> Hashtbl.fold f views e

    method copy foc_q =
      {< place = store#place foc_q;
	 focus_query = foc_q;
	 views =
         let ht = Hashtbl.create (Hashtbl.length views) in
	 Hashtbl.iter (fun v sv -> Hashtbl.add ht v sv#copy) views;
	 ht;>}

    initializer
      Hashtbl.add views (ViewClass Expr.Thing) (new state_view)
  end

class history =
  object (self)
    val mutable store : Main.store =
      try
	let uri = Sys.argv.(1) in
	if not (Sys.file_exists uri) then begin
	  let out = open_out uri in
	  output_string out
	    "<?xml version=\"1.0\"?>\n<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n</rdf:RDF>\n";
	  close_out out
	end;
	Owlapi.debug (fun () -> Main.from_path uri)
      with _ -> failwith "Missing ontology path on the command line"
    val st_backward : state Stack.t = Stack.create ()
    val st_forward : state Stack.t = Stack.create ()

    method store = store

    method root : Expr.focus_cls = [], Expr.Thing

    method home : Expr.focus_cls = self#root

    method current = Stack.top st_backward

    method init_store path =
      store <- Main.from_path path;
      Stack.clear st_backward;
      Stack.clear st_forward;
      Stack.push (new state self#store self#home) st_backward

    method cd foc_q =
      Stack.clear st_forward;
      let st = self # current # copy foc_q in
      Stack.push st st_backward

    method reset foc_q =
      Stack.clear st_forward;
      Stack.push (new state self#store foc_q) st_backward

    method backward =
      if Stack.length st_backward > 1
      then begin
	let s = Stack.pop st_backward in
	Stack.push s st_forward
      end

    method forward =
      if Stack.length st_forward > 0
      then begin
	let s = Stack.pop st_forward in
	Stack.push s st_backward
      end

    initializer
      Stack.push (new state self#store self#home) st_backward
  end

let history = new history

let syntax = ref Expr.print_manchester

let string_of_class v c = Main.string_of_class (if v = ViewExtent then Expr.print_lis else !syntax) c

let tree_of_focus_class ps_c = Main.tree_of_focus_class !syntax ps_c

let rec markup_of_tree = function
  | [] -> ""
  | Main.Tag (x, l1)::l ->
    let attributes =
      match x with
      | Expr.Tag_modifier -> ["foreground", str_color_modifier]
      | Expr.Tag_class -> ["foreground", str_color_class]
      | Expr.Tag_property -> ["foreground", str_color_property]
      | Expr.Tag_individual -> ["foreground", str_color_entity]
      | Expr.Tag_path ps -> [] in (* not used in increments *)
    let contents = markup_of_tree l1 in
    let head =
      if attributes = []
      then contents
      else "<span " ^ String.concat ~sep:" " (List.map (fun (a,v) -> a ^ "=\"" ^ v ^ "\"") attributes) ^ ">" ^ contents ^ "</span>" in
    head ^ markup_of_tree l
  | Main.Str s::l -> Glib.Markup.escape_text s ^ markup_of_tree l
  | Main.Image uri::l -> markup_of_tree l (* images ignored in increments *)
  | Main.Focus::l -> markup_of_tree l (* focus irrelevant in increments *)

let markup_of_class v c =
  markup_of_tree (Main.tree_of_class (if v = ViewExtent then Expr.print_lis else !syntax) c)


let scale_of_ratio n m =
  let max_scale = 1.5 in
  let min_scale = 0.8 in
  min_scale +.
    (max_scale -. min_scale) *.
    log10 (float_of_int (2 + n)) /. log10 (float_of_int (2 + m))
    
let color_of_ratio n m = (* assuming 0 <= n <= m *)
  let max_intensity = 65535. in
  let min_intensity = 16000. in
  let rgb = int_of_float (
    if m = 0 then max_intensity
    else if n = 0 then min_intensity
    else
      min_intensity +.
	(max_intensity -. min_intensity) *.
	log10 (float_of_int (2 * n)) /. log10 (float_of_int (2 * m))) in
  str_color_of_rgb (rgb,rgb,rgb) (* Gdk.Color.alloc ~colormap (`RGB (rgb,rgb,rgb)) *)

let incr_of_link v link =
  let c = link.Main.link_cls in
  { incr_cls = c;
    incr_str = string_of_class v c;
    incr_markup = markup_of_class v c;
    incr_weight = 400; (* if link.Main.link_necessary then 400 else 200;*)
    incr_scale = if link.Main.link_necessary then 1.5 else 1.0;
    incr_color = str_black;
    incr_bg_color = str_white; (*if link.Main.link_necessary then str_color_necessary else str_color_satisfiable;*)
    incr_iri_opt = iri_of_class c
  }

let rec ls root sv (v : id_view) : incr list = Common.prof "ls" (fun () ->
  match v with
  | ViewClass x ->
      let links = history#current#place#get_sublinks x in
      let incrs = List.map ~f:(incr_of_link v) links in
      List.sort ~cmp:sv#links_sort_fun incrs
  | ViewExtent ->
      let links = history#current#place#get_instances in
      let incrs = List.map ~f:(incr_of_link v) links in
      List.sort ~cmp:sv#links_sort_fun incrs)

let ls_opt force root sv (v : id_view) =
  if force || sv # links_opt = None
  then begin
    let incrs = ls root sv v in
    sv # set_links_opt (Some incrs);
    incrs end
  else match sv # links_opt with
  | Some incrs -> incrs
  | None -> assert false


(* building the interface *)

let accel_group = GtkData.AccelGroup.create ()

let tooltips = GData.tooltips ()

let title () = "Possible World Explorer (PEW)"

let window = GWindow.window ~title:(title ()) ()

let vbox = GPack.vbox ~border_width:8 ~spacing:8 ~packing:window#add ()

let menubar = GMenu.menu_bar ~packing:vbox#pack ()
let menubar_factory = new GMenu.factory menubar

let file_menu = menubar_factory#add_submenu "File"
let file_menu_factory = new GMenu.factory file_menu
let cmd_new = file_menu_factory#add_item "New..."
let cmd_open = file_menu_factory#add_item "Open..."
let cmd_save = file_menu_factory#add_item ~key:_s "Save"
let cmd_saveas = file_menu_factory#add_item "Save as..."
(*
   let cmd_close = file_menu_factory#add_item "Close"
let _ = file_menu_factory#add_separator ()
let import_file_menu = file_menu_factory#add_submenu "Import"
let import_file_factory = new GMenu.factory import_file_menu
let cmd_import_from_rdf = import_file_factory#add_item "Import RDF..."
(*let cmd_import_from_xml = import_file_factory#add_item "Import XML..."*)
let cmd_import_from_uri = import_file_factory#add_item "Import URI..."
let cmd_import_from_path = import_file_factory#add_item "Import from path..."
let export_file_menu = file_menu_factory#add_submenu "Export"
let export_file_factory = new GMenu.factory export_file_menu
let cmd_export_to_rdf = export_file_factory#add_item "Export RDF..."
*)
let _ = file_menu_factory#add_separator ()
let cmd_stat = file_menu_factory#add_item "Statistics"
let _ = file_menu_factory#add_separator ()
let cmd_quit = file_menu_factory#add_item "Quit"

let view_menu = menubar_factory#add_submenu "View"
let view_menu_factory = new GMenu.factory view_menu
(*
let cmd_back = view_menu_factory#add_item ~key:_b "Back"
let cmd_fwd = view_menu_factory#add_item ~key:_f "Forward"
let cmd_refresh = view_menu_factory#add_item ~key:_r "Refresh"
let cmd_root = view_menu_factory#add_item ~key:_0 "Root"
*)
let radio_manchester = view_menu_factory#add_radio_item ~active:true "Manchester syntax"
let radio_dl = view_menu_factory#add_radio_item ~group:radio_manchester#group "DL syntax"
let radio_lis = view_menu_factory#add_radio_item ~group:radio_manchester#group "Sewelis syntax"
let _ = view_menu_factory#add_separator ()
let check_scale = view_menu_factory#add_check_item ~active:true "Variable font size"
let check_feat_color = view_menu_factory#add_check_item ~active:true "Foreground color"
(*
let check_lift = view_menu_factory#add_check_item ~active:false "Total count"
let check_lift_color = view_menu_factory#add_check_item ~active:true "Total count grey level"
let check_ext_color = view_menu_factory#add_check_item ~active:true "Equal extent colors"
let check_ext_preselect = view_menu_factory#add_check_item ~active:false "Objects preselection"
let check_int_preselect = view_menu_factory#add_check_item ~active:false "Features preselection"
*)
let _ = view_menu_factory#add_separator ()
let cmd_expand = view_menu_factory#add_item "Expand..."
let cmd_collapse = view_menu_factory#add_item "Collapse"

let help_menu = menubar_factory#add_submenu "Help"
let help_menu_factory = new GMenu.factory help_menu
let cmd_about = help_menu_factory#add_item "About Possible World Explorer..."

let hbox_buttons = GPack.hbox ~spacing:8 ~height:30 ~packing:vbox#pack ()
let button_back = GButton.button ~stock:`GO_BACK (*`STOCK " Back "*) ~packing:(hbox_buttons#pack) ()
let button_fwd = GButton.button ~stock:`GO_FORWARD (*`STOCK " Forward "*) ~packing:(hbox_buttons#pack) ()
let button_refresh = GButton.button ~stock:`REFRESH (*`STOCK " Refresh "*) ~packing:(hbox_buttons#pack) ()
let button_root = GButton.button ~stock:`HOME (*`STOCK " Root "*) ~packing:(hbox_buttons#pack) ()
(*
let button_home = GButton.button ~stock:(`STOCK " Home ") ~packing:(hbox_buttons#pack) ()
let button_bookmarks = GButton.button ~stock:(`STOCK " Bookmarks ") ~packing:(hbox_buttons#pack) ()
let button_todo = GButton.button ~stock:(`STOCK " Todo ") ~packing:(hbox_buttons#pack) ()
*)
let _ = GMisc.separator `VERTICAL ~packing:(hbox_buttons#pack) ()
let button_undo = GButton.button ~stock:`UNDO ~packing:(hbox_buttons#pack) ()
let button_save = GButton.button ~stock:`SAVE (*`STOCK " Save "*) ~packing:(hbox_buttons#pack) ()

type navigation_mode = Name | Default | Zoom | Pivot | Cross

class text_query_callbacks =
  object
    method ctx_menu : GMenu.menu -> unit = fun _ -> ()
    method focus : stat:bool -> Expr.focus -> unit = fun ~stat _ -> ()
    method transf : (Expr.focus_cls -> Expr.focus_cls) -> unit = fun _  -> ()
  end

class text_query ~width_chars
  ~(pack : ?from:Gtk.Tags.pack_type -> ?expand:bool -> ?fill:bool -> ?padding:int -> GObj.widget -> unit)
  ~focus_widget =
  (* the query buffer *)
  let tag_table = GText.tag_table () in
  let buffer = GText.buffer ~tag_table:tag_table () in
  let tag_scale_up = buffer # create_tag [`SCALE (`CUSTOM 1.5)] in
  (*      let tag_foreground_red = buffer # create_tag [`FOREGROUND str_darkred] in *)
  let tag_modifier = buffer # create_tag [`FOREGROUND str_color_modifier] in
  let tag_class = buffer # create_tag [`FOREGROUND str_color_class] in
  let tag_literal = buffer # create_tag [`FOREGROUND str_color_literal] in
  let tag_entity = buffer # create_tag [`FOREGROUND str_color_entity] in
  let tag_property = buffer # create_tag [`FOREGROUND str_color_property] in
  let tag_functor = buffer # create_tag [`FOREGROUND str_color_functor] in
  let tag_necessary = buffer # create_tag [`BACKGROUND str_color_necessary] in
  let tag_satisfiable = buffer # create_tag [`BACKGROUND str_color_satisfiable] in
  let tag_unsatisfiable = buffer # create_tag [`BACKGROUND str_color_unsatisfiable] in
  (* graphical display *)
  let sw = GBin.scrolled_window ~shadow_type:`ETCHED_IN
      ~hpolicy:`AUTOMATIC ~vpolicy:`AUTOMATIC ~packing:(fun w -> pack ~expand:true ~fill:true w) () in
  let view = GText.view ~buffer:buffer
      ~editable:false
      ~cursor_visible:true
      ~wrap_mode:`NONE (* `WORD *)
      ~width:width_chars
      ~height:2
      ~packing:(*fun w -> hbox#pack ~expand:true ~fill:true w*)  sw#add () in
(*
  let vbox_buttons = GPack.vbox ~spacing:8 ~packing:hbox#pack () in
(*
   let hbox_buttons = GPack.hbox ~spacing:8 ~packing:vbox#pack () in
   let hbox_buttons1 = GPack.hbox ~spacing:8 ~packing:vbox#pack () in
   let hbox_buttons2 = GPack.hbox ~spacing:8 ~packing:vbox#pack () in
 *)
  let button_union = GButton.button ~label:"_\n or ?" ~packing:vbox_buttons#pack () in
  let button_compl = GButton.button ~label:"not _" ~packing:vbox_buttons#pack () in
  let button_select = GButton.button ~label:"Select" ~packing:vbox_buttons#pack () in
  let button_delete = GButton.button ~label:"Delete" ~packing:vbox_buttons#pack () in
  let button_reverse = GButton.button ~label:"Reverse" ~packing:vbox_buttons#pack () in
*)
  object (self)
    val mutable clipboard = Expr.Thing
    val h_path_offsets = Hashtbl.create 7

    val mutable callbacks = new text_query_callbacks

    method connect c = callbacks <- c

    val mutable focus_on_extent = false

    method set_focus_on_extent b =
      focus_on_extent <- b

    method transf f () =
      callbacks#transf f

    method copy =
      clipboard <- fst (Expr.get_context history#current#focus_query)

    method cut =
      clipboard <- fst (Expr.get_context history#current#focus_query);
      self#transf Expr.focus_delete ()

    method paste =
      self#transf (fun foc_q -> Expr.focus_zoom history#store foc_q clipboard) ()

    method button_press ev =
      let button = GdkEvent.Button.button ev in
      if (button = 1 || button = 2) then
	try
	  let win =
	    match view#get_window `WIDGET with
	    | None -> assert false
	    | Some w -> w in
	  let x,y = Gdk.Window.get_pointer_location win in
	  let b_x,b_y = view#window_to_buffer_coords ~tag:`WIDGET ~x ~y in
	  let iter = view#get_iter_at_location ~x:b_x ~y:b_y in
	  let offset = iter#offset in
	  let path_opt, _ =
	    Hashtbl.fold
	      (fun ps (s,e) (path_opt,delta_opt) ->
		let d = e - s in
		if s <= offset && offset < e && Option.fold (fun delta -> d < delta) true delta_opt
		then (Some ps, Some d)
		else (path_opt,delta_opt))
	      h_path_offsets (None,None) in
	  Option.iter (fun path -> callbacks#focus ~stat:(button = 2) path) path_opt;
	  false
	with _ -> false
      else if button = 3 then begin
	let menu = GMenu.menu () in
	callbacks#ctx_menu menu;
	menu#popup ~button:button ~time:(GdkEvent.Button.time ev);
	true end
      else false


    val mutable focus_box = GPack.hbox ()
    initializer focus_box#add focus_widget#coerce

    method refresh = Common.prof "Gui.text_query#refresh" (fun () ->
      let foc, q = history#current#focus_query in
      let focus_mark = ref None in
      let rec print = function
	| [] -> ()
	| Main.Tag (x, l1)::l ->
	    let start_offset = buffer#end_iter#offset in
	    print l1;
	    let end_offset = buffer#end_iter#offset in
	    let tag_opt =
	      match x with
	      | Expr.Tag_modifier -> Some tag_modifier
	      | Expr.Tag_class -> Some tag_class
	      | Expr.Tag_property -> Some tag_property
	      | Expr.Tag_individual -> Some tag_entity
	      | Expr.Tag_path ps ->
		  Hashtbl.add h_path_offsets ps (start_offset, end_offset);
		  let tag_opt =
		    if ps = foc then begin
		      let start_iter = buffer#get_iter (`OFFSET start_offset) in
		      focus_mark := Some (buffer#create_mark start_iter);
		      Some
			(if history#current#place#is_satisfiable
			then
			  if history#current#place#is_necessary_at_focus
			  then tag_necessary
			  else tag_satisfiable
			else
			  tag_unsatisfiable) end
		    else None in
		  tag_opt in
	    Option.iter
	      (fun tag ->
		buffer#apply_tag tag ~start:(buffer#get_iter (`OFFSET start_offset)) ~stop:buffer#end_iter)
	      tag_opt;
	    print l
	| Main.Str s::l ->
	    buffer#insert s;
	    print l
	| Main.Image uri::l ->
	    begin
	      try buffer # insert_pixbuf ~iter:buffer#end_iter ~pixbuf:(pixbuf_of_picture ~maxside:100 uri)
	      with _ -> () (* in case the picture cannot be found *)
	    end;
	    print l
	| Main.Focus::l ->
	    let focus_anchor = buffer#create_child_anchor buffer#end_iter in
	    view#add_child_at_anchor focus_box#coerce focus_anchor;
	    print l
      in
      Hashtbl.clear h_path_offsets;
      focus_box#remove focus_widget#coerce;
      focus_box <- GPack.hbox ();
      focus_box#add focus_widget#coerce;
      buffer # set_text "";
      print (tree_of_focus_class (foc,q));
      buffer # apply_tag tag_scale_up ~start:buffer#start_iter ~stop:buffer#end_iter;
      Option.iter (fun mark -> view#scroll_to_mark (`MARK mark)) !focus_mark;
      focus_widget#grab_focus ();
      ())

    initializer
      (* callbacks *)
      ignore (view#connect#delete_from_cursor ~callback:(fun _ _ ->
	self#transf Expr.focus_delete ()));
      ignore (view#connect#copy_clipboard ~callback:(fun _ -> self#copy));
      ignore (view#connect#cut_clipboard ~callback:(fun _ -> self#cut));
      ignore (view#connect#paste_clipboard ~callback:(fun _ -> self#paste));
(*
      button_union # connect # clicked ~callback:
	(fun () ->
	  self#transf Expr.focus_union ());
      button_compl # connect # clicked ~callback:
	(fun () ->
	  if history#current#place#is_necessary_at_focus
	  then error_message "The complement is not applicable here. This would make the current description unsatisfiable"
	  else self#transf Expr.focus_compl ());
      button_select # connect # clicked ~callback:
	(fun () ->
	  self#transf Expr.focus_select ());
      button_delete # connect # clicked ~callback:
	(fun () ->
	  self#transf Expr.focus_delete ());
      button_reverse # connect # clicked ~callback:
	(fun () ->
	  self#transf (fun foc_q -> [], Expr.focus_reversal foc_q) ());
*)
      view # event # connect # button_press ~callback:self#button_press;
      ()

      (* tooltips *)
(*
   tooltips#set_tip view#coerce
   ~text:"This is the query area that you can edit directly or by activating features in the navigation window";
 *)
(*
      tooltips#set_tip button_union#coerce
	~text:"Add or create an alternative (or) to the current query focus";
      tooltips#set_tip button_compl#coerce
	~text:"Applies negation to the current query focus, if satisfiable";
      tooltips#set_tip button_select#coerce
	~text:"Set the current query focus as the whole query";
      tooltips#set_tip button_delete#coerce
	~text:"Delete the current query focus (it will be replaced by ?)";
      tooltips#set_tip button_reverse#coerce
	~text:"Reformulate the current query from its focus"
*)
  end

class entry_feature_callbacks =
  object
    method cd (x : Expr.cls) = ()
    method cd_input ~(mode:navigation_mode) ~(custom:string) = true
  end

class entry_feature ?(full = true) ?pack () =
  let box = GPack.hbox ~spacing:8 ?packing:pack () in
  let opt pack = if full then Some pack else None in
  (* menu for creating new resources *)
(* TODO *)
  let menubar = GMenu.menu_bar
      ~packing:(fun w -> box#pack ?from:(if full then None else Some `END) w) (*opt (fun w -> box#pack w)*)  () in
  let menubar_factory = new GMenu.factory menubar in
  let menu_create = menubar_factory#add_submenu "Create" in
  let menu_create_factory = new GMenu.factory menu_create in
(*
  let item_var = menu_create_factory#add_item "a variable" in
*)
  let item_uri = menu_create_factory#add_item "an entity" in
  let item_class = menu_create_factory#add_item "a class" in
  let item_prop = menu_create_factory#add_item "a property" in
  let item_prop_inv = menu_create_factory#add_item "an inverse property" in
(*
  let _ = menu_create_factory#add_separator () in
  let item_plain = menu_create_factory#add_item "a text" in
  let item_date = menu_create_factory#add_item "a date (yyyy-mm-dd)" in
  let item_dateTime = menu_create_factory#add_item "a date and time (yyyy-mm-ddThh:mm:ss)" in
  let item_file = menu_create_factory#add_item "a filename (file:///...)" in
  let _ = menu_create_factory#add_separator () in
  let item_struct = menu_create_factory#add_item "a structure (...(...))" in
*)
  (* custom entry and completions *)
  let _ = GMisc.label ~text:"Search" ?packing:(opt (fun w -> box#pack w)) () in
  let entry_custom = GEdit.entry (*~width_chars:30*) ~packing:(box#pack ~expand:true) () in
  let cols_completion = new GTree.column_list in
  let feature = cols_completion#add string in
  let cell_completion = GTree.cell_renderer_text [] in
  let model_completion = GTree.list_store cols_completion in
  let matcher =
    let matches str re =
      try ignore (Str.search_forward re str 0); true
      with Not_found -> false in
    let re_key key = Str.regexp_string_case_fold key in
    fun key0 str -> Common.prof "Gui.entry_feature.matcher" (fun () ->
      let len0 = String.length key0 in
      let keys = Str.split (Str.regexp "[ '\",.;]+") key0 in
      List.fold_left (* should be at least one significant key *)
	~f:(fun res key -> res && matches str (re_key key))
	~init:true
	keys) in
  let entry_custom_completion = GEdit.entry_completion ~model:model_completion ~minimum_key_length:1 ~entry:entry_custom () in
  object (self)
    val mutable callbacks = new entry_feature_callbacks
    val mutable feat_custom = ""

    method coerce = box#coerce
    method entry = entry_custom

    method connect c = callbacks <- c

    method grab_focus () = entry_custom#misc#grab_focus ()

    method refresh = ()

    method cd_input mode () =
      let custom =
	if feat_custom = ""
	then entry_custom#text
	else feat_custom in
      if callbacks#cd_input ~mode ~custom
      then entry_custom#set_text ""

    val mutable view_features : (string * Expr.cls) list = []
    initializer
    let set_view_features () =
      view_features <-
	let lf =
	  List.map
	    ~f:(fun f -> string_of_class (ViewClass Expr.Thing) f, f)
	    history#store#restrictions in
	List.sort
	  ~cmp:(fun (s1,_) (s2,_) -> Pervasives.compare (String.length s1, s1) (String.length s2, s2))
	  lf in
    set_view_features ();
    history#store#when_signature_changed ~callback:set_view_features

    method private completions key = Common.prof "Gui.entry_feature#completions" (fun () ->
      let max_compl = 10 in
      let rec completions_aux matching_names =
	let _, _, partial, res =
	  Common.fold_while
	    (fun (ns, i, partial, res) ->
	      if partial
	      then None
	      else
		match ns with
		| [] -> None
		| (s,f)::ns1 ->
		    if i = max_compl then
		      Some (ns1, i, true, res)
		    else if true || history#current#place#has_link f
		    then
		      Some (ns1, i+1, partial, (s,f)::res)
		    else
		      Some (ns1, i, partial, res))
	    (matching_names, 0, false, []) in
	List.length res < List.length matching_names (* partial *), res
      in
      let names = view_features in
      let all_matching_names = List.filter (fun (s,_) -> matcher key s) names in
      let partial, matching_names =
	if all_matching_names = []
	then false, []
	else completions_aux all_matching_names in
      let sorted_names = List.rev matching_names in
      if partial
      then sorted_names @ [("...", Expr.Thing)]
      else sorted_names)

    method private set_model_completion =
      Common.prof "Gui.entry_feature#set_model_completion" (fun () ->
	let key = entry_custom#text in
	let sorted_names = self#completions key in
	feat_custom <- "";
	entry_custom_completion#misc#freeze_notify ();
	model_completion#clear ();
	List.iter
	  (fun (s, f) ->
	    let iter = model_completion#append () in
	    model_completion#set ~row:iter ~column:feature s)
	  sorted_names);
      entry_custom_completion#misc#thaw_notify ()

    initializer
      (* creation of features *)
(* TODO *)
(*
      item_plain#connect#activate ~callback:
	(fun () ->
	  match GToolbox.input_text ~title:"Enter a text" "" with
	  | None -> ()
	  | Some text ->
	      let x = Class.Name (Rdf.Literal (text, Rdf.Plain "")) in
	      callbacks#cd x);
      item_date#connect#activate ~callback:
	(fun () ->
	  date_dialog
	    ~title:"Select a date"
	    ~callback:
	    (fun ~year ~month ~day ->
	      let x = Class.Name
		  (Rdf.Literal
		     (Name.string_date_of_year_month_day year month day,
		      Rdf.Typed Xsd.uri_date)) in
	      Logui.item Logui.uri_DateCreation (fun obs ->
		[ (Logui.uri_class, [Turtle.Typed (string_of_concept x, Logui.lisql_Class)]) ]);
	      callbacks#cd x));
      item_dateTime#connect#activate ~callback:
	(fun () ->
	  dateTime_dialog
	    ~title:"Select a date and time"
	    ~callback:
	    (fun ~year ~month ~day ~hour ~min ~sec ->
	      let x = Class.Name
		  (Rdf.Literal
		     (Name.string_date_of_year_month_day year month day ^
		      "T" ^
		      Name.string_time_of_hour_min_sec hour min sec,
		      Rdf.Typed Xsd.uri_dateTime)) in
	      Logui.item Logui.uri_DateTimeCreation (fun obs ->
		[ (Logui.uri_class, [Turtle.Typed (string_of_concept x, Logui.lisql_Class)]) ]);
	      callbacks#cd x));
      item_file#connect#activate ~callback:
	(fun () ->
	  let default =
	    Filename.concat
	      history#store#log#dirname
	      (Name.string_dateTime_of_UnixTime (Unix.gettimeofday ())) in
	  file_dialog
	    ~title:"Select a file or a directory"
	    ~filename:default
	    ~callback:
	    (fun filename ->
	      if not (Sys.file_exists filename)
		  && question "Are you sure you want to create this file ?" then
		create_file filename;
	      let x = Class.Name (Rdf.URI (Name.uri_of_file filename)) in
	      Logui.item Logui.uri_FilenameCreation (fun obs ->
		[ (Logui.uri_class, [Turtle.Typed (string_of_concept x, Logui.lisql_Class)]) ]);
	      callbacks#cd x)
	    ());
      item_var#connect#activate ~callback:(fun () ->
	fields_dialog
	  ~title:"Creation of a variable"
	  ~fields:[("variable name", `String "X")]
	  ~callback:(function
	    | [`String name] ->
		let x = Class.Var name in
		Logui.item Logui.uri_VariableCreation (fun obs ->
		  [ (Logui.uri_class, [Turtle.Typed (string_of_concept x, Logui.lisql_Class)]) ]);
		callbacks#cd x
	    | _ -> assert false));
*)
      item_uri#connect#activate ~callback:(fun () ->
	iri_dialog history#store
	  ~title:"Creation of an individual"
	  ~callback:(fun iri ->
	    let x = Expr.Individual iri in
	    (*history#current#place#declare_individual iri;*)
	    callbacks#cd x)
	  ());
      item_class#connect#activate ~callback:(fun () ->
	iri_dialog history#store
	  ~title:"Creation of a class"
	  ~callback:(fun iri ->
	    let x = Expr.Class iri in
	    (*history#current#place#declare_class iri;*)
	    callbacks#cd x)
	  ());
      item_prop#connect#activate ~callback:(fun () ->
	iri_dialog history#store
	  ~title:"Creation of an object property restriction"
	  ~callback:(fun iri ->
	    let x = Expr.ObjectExists (Expr.ObjectProperty iri, Expr.Thing) in
	    (*history#current#place#declare_object_property iri;*)
	    callbacks#cd x)
	  ());
      item_prop_inv#connect#activate ~callback:(fun () ->
	iri_dialog history#store
	  ~title:"Creation of an inverse object property restriction"
	  ~callback:(fun iri ->
	    let x = Expr.ObjectExists (Expr.Inverse (Expr.ObjectProperty iri), Expr.Thing) in
	    (*history#current#place#declare_object_property iri;*)
  	    callbacks#cd x)
	  ());
(*
      item_struct#connect#activate ~callback:
	(fun () ->
	  functor_dialog history#store
	    ~title:"Creation of a structure"
	    ~callback:(fun (uri_funct, args_arity, focus) ->
	      let x = make_struct_arg uri_funct args_arity focus in
	      Logui.item Logui.uri_StructureCreation (fun obs ->
		[ (Logui.uri_class, [Turtle.Typed (string_of_concept x, Logui.lisql_Class)]) ]);
	      callbacks#cd x)
	    ());
*)
      (* completions *)
      ignore (entry_custom#connect#changed ~callback:(fun () ->
	self#set_model_completion));
      entry_custom_completion # set_text_column feature;
      entry_custom_completion # set_match_func
	(fun key iter -> true); (* matching is done upon change in custom_entry *)
      ignore (entry_custom_completion # connect # match_selected
		~callback:(fun fmodel fiter ->
		  let iter = fmodel#convert_iter_to_child_iter fiter in
		  let s = model_completion#get ~row:iter ~column:feature in
		  let f = List.assoc s view_features in
		  callbacks#cd f;
		  entry_custom#set_text "";
		  true));
      entry_custom # set_completion entry_custom_completion;
      entry_custom # connect # activate ~callback:(self#cd_input Default);
      tooltips#set_tip entry_custom#coerce
	~text:"Enter a custom selection";
  end


class tree_features_callbacks =
  object
    method row_activated : [`Select|`AddChild|`Necessary|`Impossible] -> Gtk.tree_iter -> unit = fun mode iter -> ()
    method row_expanded : Gtk.tree_path -> unit = fun path -> ()
    method row_collapsed : Gtk.tree_path -> unit = fun path -> ()
    method ctx_menu : Expr.cls list -> GMenu.menu -> unit = fun nb menu -> ()
    method drag_and_drop : Expr.cls list -> Expr.cls -> unit = fun lx x -> ()
  end

class tree_features multi_tree ~pack root_v =
  let sw = GBin.scrolled_window ~shadow_type:`ETCHED_IN
      ~hpolicy:`AUTOMATIC ~vpolicy:`AUTOMATIC ~packing:pack () in
  let cols = new GTree.column_list in
  let feature = cols#add string in
  let markup = cols#add string in
  let row_color = cols#add string in
  let row_scale = cols#add float in
  let row_weight = cols#add int in
  let bg_color = cols#add string in
  let addchild_stockid = cols#add string in
  let model = GTree.tree_store cols in
  let h_incr = Hashtbl.create 101 in
  let view = GTree.view ~model:model ~headers_visible:false ~packing:sw#add () in
  let cell_feature = GTree.cell_renderer_text [] in
  let col_feature =
    let col =
      GTree.view_column ~title:"Feature" ()
	~renderer:(cell_feature, ["markup",markup]) in
    col # add_attribute cell_feature "scale" row_scale;
    (*    col # add_attribute cell_feature "foreground" row_color;*)
    col # add_attribute cell_feature "background" bg_color;
    col # add_attribute cell_feature "weight" row_weight;
    col # set_sizing `GROW_ONLY;
    col in
  let cell_addchild = GTree.cell_renderer_pixbuf [(*`STOCK_ID (GtkStock.convert_id `ADD)*)] in
  let col_addchild =
    let col = GTree.view_column ~title:"Add subclass" () ~renderer:(cell_addchild, []) in
    col # add_attribute cell_addchild "stock_id" addchild_stockid;
    col in
  let cell_necessary = GTree.cell_renderer_pixbuf [`STOCK_ID (if root_v = ViewExtent then "" else GtkStock.convert_id `YES)] in
  let col_necessary = GTree.view_column ~title:"Make necessary" () ~renderer:(cell_necessary, []) in
  let cell_impossible = GTree.cell_renderer_pixbuf [`STOCK_ID (if root_v = ViewExtent then "" else GtkStock.convert_id `NO)] in
  let col_impossible = GTree.view_column ~title:"Make impossible" () ~renderer:(cell_impossible, []) in
  
  let function_top = "<top>" in
  let function_up = "<up>" in
  let function_down = "<down>" in
  let function_bottom = "<bottom>" in
  object (self)
    val mutable callbacks = new tree_features_callbacks

    method connect c = callbacks <- c

    method widget = (sw :> GObj.widget)

    method root = root_v

    val mutable ignore_selection_changed = false

    val mutable drag_selection : Expr.cls list = []

    method selection_changed () =
      multi_tree#set_focus root_v

    method row_activated path vcol =
      let iter = model # get_iter path in
      let sf = self # feature_from_path path in
      if sf = function_top then self # page_top (model#iter_parent iter)
      else if sf = function_up then self # page_up (model#iter_parent iter)
      else if sf = function_down then self # page_down (model#iter_parent iter)
      else if sf = function_bottom then self # page_bottom (model#iter_parent iter)
      else
	let mode =
	  if vcol#get_oid = col_addchild#get_oid then `AddChild
	  else if vcol#get_oid = col_necessary#get_oid then `Necessary
	  else if vcol#get_oid = col_impossible#get_oid then `Impossible
	  else `Select in
	callbacks # row_activated mode iter

    method button_press ev =
      let path_opt =
	let x = int_of_float (GdkEvent.Button.x ev) in
	let y = int_of_float (GdkEvent.Button.y ev) in
	match view#get_path_at_pos ~x ~y with
	| None -> None
	| Some (path, _, _, _) -> Some path in
      let button = GdkEvent.Button.button ev in
      if button = 3 then
	let selection = view#selection in
	let nb = selection#count_selected_rows in
	if nb = 0 then begin
	  Option.iter
	    (fun path ->
	      selection#unselect_all ();
	      selection#select_path path)
	    path_opt
	end;
	let menu = GMenu.menu () in
	let lx = self#selected_concepts in
	callbacks # ctx_menu lx menu;
	menu#popup ~button:(GdkEvent.Button.button ev) ~time:(GdkEvent.Button.time ev);
	true
      else if button = 2 then
	match path_opt with
	| None -> false
	| Some path -> self#expand_row path; true
      else
	false

    method private feature_from_iter tree_iter =
      model#get ~row:tree_iter ~column:feature

    method feature_from_path path : string =
      model#get ~row:(model#get_iter path) ~column:feature

    method markup_from_path path : string =
      model#get ~row:(model#get_iter path) ~column:markup

    method incr_from_iter iter : incr =
      let v = model#get ~row:iter ~column:feature in
      Hashtbl.find h_incr v

    method incr_from_path path : incr =
      self#incr_from_iter (model#get_iter path)

    method class_from_iter iter : Expr.cls =
     (self#incr_from_iter iter).incr_cls

    method class_from_path path : Expr.cls =
      let i = self#incr_from_path path in
      i.incr_cls

    method state_view = history#current#get_view root_v

	(* get the state_view of the path, and create it if necessary *)
    method state_view_from_path path =
      let st = history # current in
      let views = st # views in
      let i = self # incr_from_path path in
      let v = ViewClass i.incr_cls in
      try
	Hashtbl.find views v
      with _ ->
	let sv =
	  try
	    match model#iter_parent (model#get_iter path) with
	    | None -> st # copy_view (self # state_view)
	    | Some iter ->
		let v = ViewClass (self#class_from_iter iter) in
		st # copy_view (Hashtbl.find views v)
	  with _ -> (* should not happen *)
	    st # new_view in
	Hashtbl.add views v sv;
	sv

    method private row_color_from_incr i =
      if not check_feat_color#active
      then str_black
      else
	match kind_of_class i.incr_cls with
	| Entity -> str_color_entity
	| Class -> str_color_class
	| Property | InverseProperty -> str_color_property
	| _ -> str_black

    method private row_scale_from_incr i =
      if not check_scale#active
      then 1.
      else i.incr_scale

    method private row_weight_from_incr i =
      i.incr_weight

    method private bg_color_from_incr i =
      i.incr_bg_color

    method selected_paths = view # selection # get_selected_rows

    method selected_concepts =
      let paths = self#selected_paths in
      List.map self#class_from_path paths

    method private refresh_function_iter iter (s : string) : unit =
      model#set ~row:iter ~column:feature s;
      model#set ~row:iter ~column:markup s;
      model#set ~row:iter ~column:row_color str_darkred;
      model#set ~row:iter ~column:row_scale 1.;
      model#set ~row:iter ~column:row_weight 400

    method private refresh_tree_iter iter i : unit = Common.prof "refresh_tree_iter" (fun () ->
      Hashtbl.replace h_incr i.incr_str i;
      model#set ~row:iter ~column:feature i.incr_str;
      model#set ~row:iter ~column:markup i.incr_markup;
      model#set ~row:iter ~column:row_color (self # row_color_from_incr i);
      model#set ~row:iter ~column:bg_color (self # bg_color_from_incr i);
      model#set ~row:iter ~column:row_scale (self # row_scale_from_incr i);
      model#set ~row:iter ~column:row_weight (self # row_weight_from_incr i);
      model#set ~row:iter ~column:addchild_stockid
	( match i.incr_cls with
	| Expr.Class _
	| Expr.ObjectExists (_, Expr.Thing) -> GtkStock.convert_id `ADD
	| _ -> "" (*GtkStock.convert_id `NONE*) ))

    method refresh_view force parent_opt v sv = Common.prof "Gui.tree_features#refresh_view" (fun () ->
      let page_start, page_size = sv#links_page_start, sv#links_page_size in
      let incrs = ls_opt force (parent_opt = None) sv v in
      let nb_incrs = List.length incrs in
      let visible_incrs = Common.sub_list incrs (page_start - 1) page_size in
      sv # set_expanded true;
      let expd = false (* (List.length visible_incrs) = 1 *) in
      let n_children = model#iter_n_children parent_opt in
      let pos = ref (-1) in
      let get_iter () = Common.prof "Gui.get_iter" (fun () ->
	Pervasives.incr pos;
	if !pos < n_children
	then model#iter_children ~nth:!pos parent_opt
	else model#append ?parent:parent_opt ()) in
      if nb_incrs > page_size then
	self # refresh_function_iter (get_iter ())
	  (if page_start = 1
	  then function_bottom
	  else function_up);
      List.iter
	(fun i ->
	  let iter' = get_iter () in (* TODO: try to set iter' as a reference out of the loop *)
	  self # refresh_tree_iter iter' i;
	  self # expand_view expd force iter' (model#get_path iter') i)
	visible_incrs;
      if nb_incrs > page_size then
	self # refresh_function_iter (get_iter ())
	  (if nb_incrs > page_start - 1 + page_size
	  then function_down
	  else function_top);
      for i = n_children - 1 downto !pos + 1 do
	ignore (model#remove (model#iter_children ~nth:i parent_opt))
      done)

    method private expand_view expd force tree_iter path i = Common.prof "Gui.tree_features#expand_view" (fun () ->
      ( match i.incr_cls with
      | Expr.Class _
      | Expr.ObjectExists _ ->
	  model#set ~row:(model#append ~parent:tree_iter ()) ~column:feature ""
      | _ -> ());
      let st = history # current in
      let views = st # views in
      let v = ViewClass i.incr_cls in
      let sv_opt =
	try
	  let sv = Hashtbl.find views v in
	  if sv # expanded
	  then Some sv
	  else None
	with _ ->
	  if expd  (* if we must expand tree_iter *)
	  then begin (* then we create a stateview for tree_iter in expanded_views *)
	    let sv =
	      try
		match model#iter_parent tree_iter with
		| None -> st # copy_view (self # state_view)
		| Some iter ->
		    let v = ViewClass (self#class_from_iter iter) in
		    st # copy_view (Hashtbl.find views v)
	      with _ -> (* should not happen *)
		st # new_view in
	    Hashtbl.add views v sv;
	    Some sv end
	  else None in
      match sv_opt with
      | None ->
	  view # collapse_row path
      | Some sv ->
	  ignore (self#refresh_view force (Some tree_iter) v sv);
	  view # expand_row path)

    method refresh force =  (* when force is true, links are re-computed *)
      Common.prof "Gui.tree_features#refresh" (fun () ->
	view#selection#unselect_all ();
	model#clear ();
	self#refresh_view force None root_v
	  (history#current#get_view root_v))

    method expand_row ?sort_fun ?page_size path =
      let v = ViewClass (self # class_from_path path) in
(*      let i = self # incr_from_path path in *)
      let sv = self # state_view_from_path path in
      Common.iter_option (fun sort_fun -> sv # set_links_sort_fun sort_fun) sort_fun;
      Common.iter_option (fun page_size -> sv # set_links_page_size page_size) page_size;
      self # refresh_view false (Some (model#get_iter path)) v sv;
      view # expand_row path

    method collapse_row path =
      let v = ViewClass (self#class_from_path path) in
      let views = history # current # views in
      (try (Hashtbl.find views v) # set_expanded false with Not_found -> ());
      view # collapse_row path

    method page_top parent_opt =
      let v = Common.fold_option (fun iter -> ViewClass (self # class_from_iter iter)) root_v parent_opt in
      let sv = Hashtbl.find history#current#views v in
      let new_offset = 1 in
      sv # set_links_page_start new_offset;
      match parent_opt with
      | None -> self # refresh false
      | Some iter -> self # refresh_view false (Some iter) v sv

    method page_up parent_opt =
      let v = Common.fold_option (fun iter -> ViewClass (self # class_from_iter iter)) root_v parent_opt in
      let sv = Hashtbl.find history#current#views v in
      let offset = sv # links_page_start in
      let maxobj = sv # links_page_size in
      let new_offset =
	let no = offset - maxobj in
	if no < 1 then 1 else no in
      sv # set_links_page_start new_offset;
      match parent_opt with
      | None -> self # refresh false
      | Some iter -> self # refresh_view false (Some iter) v sv

    method page_down parent_opt =
      let v = Common.fold_option (fun iter -> ViewClass (self # class_from_iter iter)) root_v parent_opt in
      let sv = Hashtbl.find history#current#views v in
      let offset = sv # links_page_start in
      let maxobj = sv # links_page_size in
      let nbobj = Common.fold_option (fun l -> List.length l) 0 sv#links_opt in
      let new_offset =
	let no = offset + maxobj in
	if no > nbobj then offset else no in
      sv # set_links_page_start new_offset;
      match parent_opt with
      | None -> self # refresh false
      | Some iter -> self # refresh_view false (Some iter) v sv

    method page_bottom parent_opt =
      let v = Common.fold_option (fun iter -> ViewClass (self # class_from_iter iter)) root_v parent_opt in
      let sv = Hashtbl.find history#current#views v in
      let maxobj = sv # links_page_size in
      let nbobj = Common.fold_option (fun l -> List.length l) 0 sv#links_opt in
      let new_offset = ((nbobj - 1) / maxobj) * maxobj + 1 in
      sv # set_links_page_start new_offset;
      match parent_opt with
      | None -> self # refresh false
      | Some iter -> self # refresh_view false (Some iter) v sv

    initializer
      view#selection#set_mode `MULTIPLE;
      view#append_column col_addchild;
      view#append_column col_feature;
      view#append_column col_necessary;
      view#append_column col_impossible;
      view#set_expander_column (Some col_feature);

      view#selection#connect#after#changed ~callback:self#selection_changed;
      view#connect#after#row_activated ~callback:self#row_activated;
      view#connect#row_expanded ~callback:(fun _ path -> self # expand_row path; callbacks # row_expanded path);
      view#connect#row_collapsed ~callback:(fun _ path -> self # collapse_row path; callbacks # row_collapsed path);
      view#event#connect#button_press ~callback:self#button_press;
      view#drag#source_set ~modi:[`BUTTON1] ~actions:[`MOVE]
	[ { Gtk.target = "STRING"; Gtk.flags = []; Gtk.info = 0} ];
      view#drag#connect#data_get ~callback:(fun _ sel ~info ~time ->
	let paths = view#selection#get_selected_rows in
	let lc1 = List.map (fun path -> self#class_from_path path) paths in
	drag_selection <- lc1;
	view#selection#unselect_all ();
	sel#return "");
      view#drag#dest_set ~actions:[`MOVE]
	[ { Gtk.target = "STRING"; Gtk.flags = []; Gtk.info = 0} ];
      view#drag#connect#data_received ~callback:(fun context ~x ~y sel ~info ~time ->
	GtkSignal.stop_emit ();
	begin
	  try
	    view#selection#unselect_all ();
	    ( match view#get_path_at_pos ~x ~y with
	    | Some (path,_,_,_) ->
	      let c2 = self#class_from_path path in
	      callbacks#drag_and_drop drag_selection c2
	    | None -> () )
	  with _ -> ()
	end;
	context#finish ~success:true ~del:false ~time);
      view#drag#connect#data_delete ~callback:(fun context ->
	GtkSignal.stop_emit ());
      ()
  end

class multi_tree_features navlink ~pack_extent ~pack =
  let paned_extent = GPack.paned `HORIZONTAL (*~packing:pack*) () in
  let paned_facets = GPack.paned `VERTICAL
(*      ~packing:(paned_extent#pack1 ~resize:true ~shrink:true) *)
      () in
  let nb = GPack.notebook
      ~enable_popup:true
      ~scrollable:true
      ~tab_pos:`TOP (*`LEFT*)
      ~homogeneous_tabs:true
(*      ~packing:(paned_facets#pack2 ~resize:true ~shrink:true) *)
      () in
  let ht = Hashtbl.create 10 in
  let ht_page = Hashtbl.create 10 in
  object (self)
    val mutable callbacks = new tree_features_callbacks
    val mutable focus : int = -1

    method set_ratio_facets n d = paned_set_ratio paned_facets n d

    method set_ratio_extent n d = paned_set_ratio paned_extent n d

    method connect c =
      callbacks <- c;
      Hashtbl.iter (fun i tf -> tf#connect c) ht

    method get_tree i =
      try Hashtbl.find ht i
      with _ -> failwith ("Gui.multi_tree#get_tree " ^ string_of_int i)

    method facets = self#get_tree (-1)

    method extent = self#get_tree (-2)

    method set_focus v : unit =
      focus <-
	if v = ViewClass Expr.Thing then (-1)
	else if v = ViewExtent then (-2)
	else
	  (try Hashtbl.find ht_page v
	  with Not_found -> (-1));
      navlink#set_focus_on_extent false

    method current =
      self#get_tree focus

    method feature_from_path = self # current # feature_from_path

    method markup_from_path = self # current # markup_from_path

    method class_from_iter = self # current # class_from_iter

    method class_from_path = self # current # class_from_path

    method state_view = self # current # state_view

    method state_view_from_path = self # current # state_view_from_path

    method selected_paths = self # current # selected_paths

    method selected_concepts = self # current # selected_concepts

    method refresh force =
      let current_v =
	try (self#get_tree (nb#current_page))#root
	with _ -> assert false in
      for i = Hashtbl.length ht_page - 1 downto 0 do
	nb#remove_page i;
	Hashtbl.remove ht i
      done;
      Hashtbl.clear ht_page;
      Common.prof "Gui.navig#refresh/extent" (fun () -> self # extent # refresh force);
      Common.prof "Gui.navig#refresh/facets" (fun () -> self # facets # refresh force);
      try
	let pos = Hashtbl.find ht_page current_v in
	nb#goto_page pos
      with _ -> ()

    method expand_row = self # current # expand_row

    method collapse_row = self # current # collapse_row

    method add_tab (v : id_view) =
      try
	let pos = Hashtbl.find ht_page v in
	nb#goto_page pos
      with Not_found ->
	let label_text =
	  match v with
	  | ViewClass x -> string_of_class (ViewClass Expr.Thing) x
	  | ViewExtent -> "(individuals)" in
	let label = GMisc.label ~text:label_text () in
	let page = new tree_features self
	    ~pack:(fun x -> ignore (nb#append_page ~tab_label:(label :> GObj.widget) x))
	    v in
	page # connect callbacks;
	page # refresh true;
	let pos = Hashtbl.length ht_page in
	Hashtbl.add ht pos page;
	Hashtbl.add ht_page v pos;
	nb#goto_page pos

    method remove_tab = ()

    initializer
      let tree_facets = new tree_features self
	  ~pack
(*	  ~pack:(paned_extent#pack1 ~resize:true ~shrink:true) *)
	  (ViewClass Expr.Thing) in
      Hashtbl.add ht (-1) tree_facets;
      let tree_extent = new tree_features self
	  ~pack:pack_extent
(*	  ~pack:(paned_extent#pack2 ~resize:true ~shrink:true) *)
	  ViewExtent in
      Hashtbl.add ht (-2) tree_extent;
      nb # connect # switch_page ~callback:(fun i -> try (self # get_tree i) # refresh true with _ -> ());
      ()
  end


let paned_global = GPack.paned `HORIZONTAL ~packing:(vbox#pack ~expand:true ~fill:true) ()

let paned_left = GPack.paned `VERTICAL ~packing:(paned_global#pack1 ~resize:true ~shrink:false) ()

let vbox_query = GPack.vbox ~spacing:4 ~packing:(paned_left#pack1 ~resize:true ~shrink:false) ()
let label_query = GMisc.label ~text:"Satisfiable class expression" ~packing:vbox_query#pack ()
let toolbar_query = GPack.hbox ~spacing:8 ~height:30 ~packing:vbox_query#pack ()
let button_declare = GButton.button ~stock:(`STOCK "_Possible") ~packing:(toolbar_query#pack) ()
let button_enforce = GButton.button ~stock:(`STOCK "_Necessary") ~packing:(toolbar_query#pack) ()
let button_retract = GButton.button ~stock:(`STOCK "_Impossible") ~packing:(toolbar_query#pack) ()
let button_name = GButton.button ~stock:(`STOCK "_Define class") ~packing:(toolbar_query#pack) ()
let focus_ef = new entry_feature ~full:false ()
let wq = new text_query
    ~width_chars:30
    ~pack:vbox_query#pack
    ~focus_widget:focus_ef

let vbox_extent = GPack.vbox ~spacing:4 ~packing:(paned_left#pack2 ~resize:true ~shrink:false) ()
let label_extent = GMisc.label ~text:"Known instances" ~packing:vbox_extent#pack ()
let toolbar_extent = GPack.hbox ~spacing:8 ~height:30 ~packing:vbox_extent#pack ()
let button_instance = GButton.button ~stock:(`STOCK "_Add named instance") ~packing:(toolbar_extent#pack) ()
let button_anonymous = GButton.button ~stock:(`STOCK "Add anonymous instance") ~packing:(toolbar_extent#pack) ()

let vbox_adjuncts = GPack.vbox ~spacing:4 ~packing:(paned_global#pack2 ~resize:true ~shrink:true) ()
let label_adjuncts = GMisc.label ~text:"Possible adjuncts" ~packing:vbox_adjuncts#pack ()
let toolbar_adjuncts = GPack.hbox ~spacing:8 ~height:30 ~packing:vbox_adjuncts#pack ()
let button_class = GButton.button ~stock:(`STOCK "Add _class") ~packing:(toolbar_adjuncts#pack) ()
let button_property = GButton.button ~stock:(`STOCK "Add _property") ~packing:(toolbar_adjuncts#pack) ()
let ef = new entry_feature ~full:true ~pack:vbox_adjuncts#pack ()
let navig = new multi_tree_features wq
    ~pack_extent:(vbox_extent#pack ~expand:true ~fill:true)
    ~pack:(vbox_adjuncts#pack ~expand:true ~fill:true)


(* global *)

let refresh_ui force =
  wq#refresh;
  ef#refresh;
  navig#refresh force

let refresh force = Common.prof "Gui.refresh" (fun () ->
  refresh_ui force)

let refresh_force () = refresh true

let open_url iri =
  let code = Sys.command ("xdg-open \"" ^ iri ^ "\"") in
  if code <> 0 then error_message "The resource could not be opened"


(* Event handling *)

let menu_new () =
  file_dialog ~title:"Create an ontology" ~filter:filter_owl ~filename:!last_dir
    ~callback:(fun path ->
      if confirm_overwrite path then begin
	history#init_store path;
	window#set_title (utf (title ()));
	refresh true end) ()

let menu_open () =
  file_dialog ~title:"Open an ontology" ~filter:filter_owl ~filename:!last_dir
    ~callback:(fun path ->
      history#init_store path;
      window#set_title (utf (title ()));
      refresh true)
    ()

let menu_undo () =
  history#store#undo_axiom;
  refresh true
    
let menu_save () =
  history#store#save

let menu_saveas () =
  file_dialog ~title:"Save an ontology" ~filter:filter_owl ~filename:!last_dir
    ~callback:(fun path ->
      if confirm_overwrite path
      then history#store#save_as path)
    ()

(*
   let menu_saveas () =
   file_dialog ~title:"Save a LIS context" ~filter:filter_lis ~filename:!last_dir
   ~callback:(fun name ->
   if confirm_overwrite name then begin
   history#save_store_as name;
   history#load_store name;
   window#set_title (utf (title ()))
   end) ()

   let menu_save () =
   match history#store#filename with
   | None -> menu_saveas ()
   | Some f -> history#save_store

   let menu_close () =
   history#init_store None;
   window#set_title (utf (title ()));
   refresh true
 *)

let menu_define_label uri_label default_lang uri () = ()
(*
  fields_dialog ~title:"Define a new label"
    ~fields:[("label", `String (preview_of_uri uri))]
    ~callback:(function
      | [`String label] ->
	  awaken
	    (fun () ->
	      if uri_label = Rdfs.uri_label then
		history#store#set_label uri label
	      else if uri_label = Lisql.Lisql.uri_inverseLabel then
		history#store#set_inverseLabel uri label
	      else assert false)
	    refresh_force
      | _ -> assert false)
*)

let menu_define_ns uri () =
  fields_dialog ~title:"Define a new namespace"
    ~fields:[("prefix",`String ""); ("URI prefix", `String uri)]
    ~callback:(function
      | [`String pre; `String ns] ->
	  let pre = if pre = "" || pre.[String.length pre - 1] <> ':' then pre ^ ":" else pre in
	  (*if Lisql.valid_string Name.parse_prefix () pre
	  then*) awaken (fun () -> history#store#add_prefix pre ns) refresh_force
	  (*else failwith "Invalid prefix"*)
      | _ -> assert false)

let menu_stat () =
  info_message "No stat available"

let menu_quit () =
  if question "Do you want to save changes before closing?" then history#store#save;
  GMain.quit ();
  Printf.printf "Profiling...\n";
  let l =
    Hashtbl.fold
      (fun s elem res -> (elem.Common.prof_time, elem.Common.prof_nb, elem.Common.prof_mem,s)::res)
      Common.tbl_prof [] in
  let l = List.sort Pervasives.compare l in
  List.iter
    (fun (t,n,m,s) -> Printf.printf "%s: %d calls, %.1f seconds\n" s n t)
    l

let menu_cd (foc,q) =
  history#cd (foc,q);
  refresh true

let menu_reset (foc,q) =
  history#reset (foc,q);
  refresh true

let menu_root () =
  menu_reset history#root

let menu_home () =
  menu_reset history#home

let menu_back () =
  history # backward;
  refresh true

let menu_fwd () =
  history # forward;
  refresh false

let menu_refresh () =
  refresh true

let focus_query ~stat foc =
(*print_endline ("focused on " ^ string_of_focus path');*)
  let _, q = history#current#focus_query in
  menu_cd (foc,q)

let query_transf_focus f =
  menu_cd (f history#current#focus_query)


let menu_expand_as_tab () =
  let paths = navig # selected_paths in
  List.iter
    (fun path ->
      let v = ViewClass (navig # class_from_path path) in
      navig # expand_row path;
      navig # add_tab v;
      navig # collapse_row path)
    paths

let menu_collapse_current_tab () =
  navig # remove_tab

let menu_expand () =
  let paths = navig # selected_paths in
  let sv =
    match paths with
    | [] -> navig # state_view
    | path::_ -> navig # state_view_from_path path in
  let w = GWindow.dialog
      ~destroy_with_parent:true
      ~title:"Expanding a view with options"
      () in
  let vbox = w#vbox in
  let page_size = let ps = sv # links_page_size in if ps = max_int then None else Some (string_of_int ps) in
  let check_page_size = GButton.check_button ~label:"maximum number of increments displayed" ~active:(page_size<>None) ~packing:vbox#add () in
  let entry_page_size = GEdit.entry ?text:page_size ~editable:(page_size<>None) ~width_chars:10 ~packing:vbox#add () in
  check_page_size#connect#toggled ~callback:(fun () -> entry_page_size#set_editable check_page_size#active);

  let action_area = w#action_area in
  let button_cancel = GButton.button ~stock:`CANCEL ~packing:action_area#add () in
  let button_ok = GButton.button ~stock:`OK ~packing:action_area#add () in
  button_cancel#connect#clicked ~callback:w#destroy;
  button_ok#connect#clicked ~callback:(fun () ->
    try
      let page_size =
	if check_page_size#active
	then int_of_string entry_page_size#text
	else max_int in
      let chsv sv =
(*	sv # set_links_sort_fun sort_fun; *)
	sv # set_links_page_size page_size in
      ( match paths with
      | [] ->
	  chsv sv;
	  navig # refresh true
      | _ ->
	  List.iter
	    (fun path ->
	      sv # set_links_opt None;
	      navig # expand_row (*~sort_fun:sort_fun*) ~page_size:page_size path)
	    paths
       );
      w#destroy ()
    with _ -> ());
  w#show ()

let menu_expand_with_options f () =
  let paths = navig # selected_paths in
  let sv =
    match paths with
    | [] -> navig # state_view
    | path::_ -> navig # state_view_from_path path in
  try
    ( match paths with
    | [] ->
	navig # refresh true
    | _ ->
	List.iter 
	  (fun path ->
	    sv # set_links_opt None;
	    navig # expand_row path)
	  paths
     );
  with _ -> ()

let menu_collapse () =
  let paths = navig # selected_paths in
  List.iter navig#collapse_row paths

let menu_and_list xs =
  let store = history#store in
  let path_q = history#current#focus_query in
  let path_q = List.fold_left ~f:(fun path_q x -> Expr.focus_zoom history#store path_q x) ~init:path_q xs in
  let path_q = Expr.focus_next_postfix path_q in
  menu_cd path_q

let menu_or_list xs =
  match xs with
  | [] -> ()
  | x1::xs1 ->
      let x =
	match x1 with
	| Expr.ObjectExists (r, _) when List.for_all (function Expr.ObjectExists (r1,_) -> r1=r | _ -> false) xs1 ->
	    let vs = List.map (function Expr.ObjectExists (_, v) -> v | _ -> assert false) xs in
	    Expr.ObjectExists (r, Expr.simpl_union vs)
	| _ ->
	    Expr.simpl_union xs in
      menu_cd (Expr.focus_zoom history#store history#current#focus_query x)


let menu_join kind lx = ()
(*
  uri_dialog history#store
    ~title:
    ( match kind with
    | Class -> "Creation of a class"
    | Property
    | InverseProperty -> "Creation of a property"
    | _ -> assert false)
    ~callback:(fun uri ->
      let x0 =
	match kind with
	| Class -> Class.has_type uri
	| Property -> Class.role Lisql.Fwd uri
	| InverseProperty -> Class.role Lisql.Bwd uri
	| _ -> assert false in
      List.iter (fun x -> history#store#add_axiom x x0) lx;
      refresh_force ())
    ()
*)

let handle_axiom_status = function
  | Main.AlreadyTrue -> error_message "This is already true."
  | Main.OK -> refresh true
  | Main.Tautology -> error_message "Not allowed. This assertion would make the ontology inconsistent."
  | Main.HasInstance -> error_message "Not allowed. There is an instance that contradicts your assertion."

let menu_add_subclass parent_iri =
  iri_dialog history#store
    ~title:"Addition of a subclass"
    ~callback:(fun child_iri ->
      history#store#add_subclass_of child_iri parent_iri;
      refresh true)
    ()

let menu_add_subproperty parent_iri =
  iri_dialog history#store
    ~title:"Addition of a subproperty"
    ~callback:(fun child_iri ->
      history#store#add_subproperty_of child_iri parent_iri;
      refresh true)
    ()

let menu_all_necessary lx =
  if List.length lx >= 1
  then handle_axiom_status (history#current#place#make_all_necessary lx)
  
let menu_all_impossible lx =
  if List.length lx >= 1
  then handle_axiom_status (history#current#place#make_all_impossible lx)

let menu_all_disjoint lx =
  if List.length lx >= 2
  then handle_axiom_status (history#store#make_all_disjoint lx)

let menu_all_subsumedby lc1 c2 =
  let errors =
    List.fold_left
      ~f:(fun errors c1 ->
	let status = history#store#make_subsumed_by c1 c2 in
	if status = Main.OK then errors else true)
      ~init:false lc1 in
  refresh true;
  if errors then error_message "Some axioms were not allowed to avoid inconsistency."

let ctx_menu_clicked lx ctx_menu =
  let nb_selected = List.length lx in
  let selected_iris = Common.mapfilter iri_of_class lx in
  let selected_iri = if lx = [] then None else iri_of_class (List.hd lx) in
  let selected_props = Common.mapfilter prop_of_class lx in
  let selected_kinds =
    List.fold_left
      ~f:(fun res x -> LSet.add (kind_of_class x) res)
      ~init:(LSet.empty ())
      lx in

  let ctx_menu_open_url = GMenu.menu_item ~label:"Open resource"
      ~show:(nb_selected = 1 && selected_iri <> None && selected_kinds = [Entity])
      ~packing:ctx_menu#append () in
(*
  let ctx_menu_define_label = GMenu.menu_item ~label:"Define label..."
      ~show:(nb_selected = 1 && selected_iri <> None)
      ~packing:ctx_menu#append () in
  let ctx_menu_define_inverse_label = GMenu.menu_item ~label:"Define inverse label..."
      ~show:(nb_selected = 1 && selected_iri <> None)
      ~packing:ctx_menu#append () in
*)
  let ctx_menu_define_ns = GMenu.menu_item ~label:"Define namespace..."
      ~show:(nb_selected = 1 && selected_iri <> None)
      ~packing:ctx_menu#append () in
  let ctx_menu_rename = GMenu.menu_item ~label:"Rename..."
    ~show:(nb_selected = 1 && selected_iri <> None)
    ~packing:ctx_menu#append () in
  if nb_selected = 1 && selected_iri <> None then
    ignore (GMenu.separator_item ~packing:ctx_menu#append ());
  let ctx_menu_add_subclass = GMenu.menu_item ~label:"Add subclass"
    ~show:(selected_iri <> None)
    ~packing:ctx_menu#append () in
  let ctx_menu_add_superclass = GMenu.menu_item ~label:"Add superclass"
    ~show:(selected_iri <> None)
    ~packing:ctx_menu#append () in
  let ctx_menu_add_subproperty = GMenu.menu_item ~label:"Add subproperty"
    ~show:(selected_iri <> None)
    ~packing:ctx_menu#append () in
  let ctx_menu_add_superproperty = GMenu.menu_item ~label:"Add superproperty"
    ~show:(selected_iri <> None)
    ~packing:ctx_menu#append () in
  let ctx_menu_add_propertyinverse = GMenu.menu_item ~label:"Add property inverse"
    ~show:(selected_props <> [] && selected_iri <> None)
    ~packing:ctx_menu#append () in
  let _ = GMenu.separator_item ~packing:ctx_menu#append () in
  let ctx_menu_all_necessary = GMenu.menu_item ~label:"All necessary"
    ~show:(nb_selected >= 1)
    ~packing:ctx_menu#append () in
  let ctx_menu_all_impossible = GMenu.menu_item ~label:"All impossible"
    ~show:(nb_selected >= 1)
    ~packing:ctx_menu#append () in
  let ctx_menu_all_disjoint = GMenu.menu_item ~label:"All disjoint"
    ~show:(nb_selected >= 2)
    ~packing:ctx_menu#append () in
  if selected_props <> [] then
    ignore (GMenu.separator_item ~packing:ctx_menu#append ());
  let ctx_menu_functional = GMenu.menu_item ~label:"All functional properties" ~show:(selected_props <> []) ~packing:ctx_menu#append () in
  let ctx_menu_inverse_functional = GMenu.menu_item ~label:"All inverse functional properties" ~show:(selected_props <> []) ~packing:ctx_menu#append () in
  let ctx_menu_reflexive = GMenu.menu_item ~label:"All reflexive properties" ~show:(selected_props <> []) ~packing:ctx_menu#append () in
  let ctx_menu_irreflexive = GMenu.menu_item ~label:"All irreflexive properties" ~show:(selected_props <> []) ~packing:ctx_menu#append () in
  let ctx_menu_symmetric = GMenu.menu_item ~label:"All symmetric properties" ~show:(selected_props <> []) ~packing:ctx_menu#append () in
  let ctx_menu_asymmetric = GMenu.menu_item ~label:"All asymmetric properties" ~show:(selected_props <> []) ~packing:ctx_menu#append () in
  let ctx_menu_transitive = GMenu.menu_item ~label:"All transitive properties" ~show:(selected_props <> []) ~packing:ctx_menu#append () in

  if selected_iris <> [] || selected_iri <> None then
    ignore (GMenu.separator_item ~packing:ctx_menu#append ());
  let ctx_menu_and = GMenu.menu_item ~label:"_ and <all of those>" ~show:(nb_selected <> 1) ~packing:ctx_menu#append () in
  let ctx_menu_or = GMenu.menu_item ~label:"_ and <one of those>" ~show:(nb_selected <> 1) ~packing:ctx_menu#append () in
(*
  let _ = GMenu.separator_item ~packing:ctx_menu#append () in
  let ctx_menu_expand_values = GMenu.menu_item ~label:"Expand on values" ~packing:ctx_menu#append () in
  let ctx_menu_expand_props = GMenu.menu_item ~label:"Expand on properties" ~packing:ctx_menu#append () in
  let ctx_menu_expand = GMenu.menu_item ~label:"Expand..." ~packing:ctx_menu#append () in
  let ctx_menu_collapse = GMenu.menu_item ~label:"Collapse" ~packing:ctx_menu#append () in
  let ctx_sep2 = GMenu.separator_item ~packing:ctx_menu#append () in
  let ctx_menu_expand_as_tab = GMenu.menu_item ~label:"Expand as tab" ~packing:ctx_menu#append () in
  let ctx_menu_collapse_current_tab = GMenu.menu_item ~label:"Collapse current tab" ~packing:ctx_menu#append () in
*)
  ctx_menu_add_subclass#connect#activate ~callback:(fun () ->
    match selected_iri with
    | None -> assert false
    | Some parent_iri -> menu_add_subclass parent_iri);
  ctx_menu_add_superclass#connect#activate ~callback:(fun () ->
    match selected_iri with
    | None -> assert false
    | Some child_iri ->
      iri_dialog history#store
	~title:"Addition of a superclass"
	~callback:(fun parent_iri ->
	  history#store#add_subclass_of child_iri parent_iri;
	  refresh true)
	());
  ctx_menu_add_subproperty#connect#activate ~callback:(fun () ->
    match selected_iri with
    | None -> assert false
    | Some parent_iri -> menu_add_subproperty parent_iri);
  ctx_menu_add_superproperty#connect#activate ~callback:(fun () ->
    match selected_iri with
    | None -> assert false
    | Some child_iri ->
      iri_dialog history#store
	~title:"Addition of a superproperty"
	~callback:(fun parent_iri ->
	  history#store#add_subproperty_of child_iri parent_iri;
	  refresh true)
	());
  ctx_menu_add_propertyinverse#connect#activate ~callback:(fun () ->
    match selected_iri with
    | None -> assert false
    | Some iri ->
      iri_dialog history#store
	~title:"Addition of an property inverse"
	~callback:(fun inverse_iri ->
	  history#store#add_property_inverse iri inverse_iri;
	  refresh true)
	());
  ctx_menu_all_necessary#connect#activate ~callback:(fun () ->
    menu_all_necessary lx);
  ctx_menu_all_impossible#connect#activate ~callback:(fun () ->
    menu_all_impossible lx);
  ctx_menu_all_disjoint#connect#activate ~callback:(fun () ->
    menu_all_disjoint lx);

  ctx_menu_functional#connect#activate ~callback:(fun () ->
    List.iter history#store#make_functional selected_props;
    refresh true);
  ctx_menu_inverse_functional#connect#activate ~callback:(fun () ->
    List.iter history#store#make_inverse_functional selected_props;
    refresh true);
  ctx_menu_reflexive#connect#activate ~callback:(fun () ->
    List.iter history#store#make_reflexive selected_props;
    refresh true);
  ctx_menu_irreflexive#connect#activate ~callback:(fun () ->
    List.iter history#store#make_irreflexive selected_props;
    refresh true);
  ctx_menu_symmetric#connect#activate ~callback:(fun () ->
    List.iter history#store#make_symmetric selected_props;
    refresh true);
  ctx_menu_asymmetric#connect#activate ~callback:(fun () ->
    List.iter history#store#make_asymmetric selected_props;
    refresh true);
  ctx_menu_transitive#connect#activate ~callback:(fun () ->
    List.iter history#store#make_transitive selected_props;
    refresh true);

  ctx_menu_and#connect#activate ~callback:(fun () -> menu_and_list lx);
  ctx_menu_or#connect#activate ~callback:(fun () -> menu_or_list lx);
(*
  ctx_menu_expand_values#connect#activate
    ~callback:(menu_expand_with_options
		 (fun l -> `Across true :: List.filter (function `Across _ -> false | _ -> true) l));
  ctx_menu_expand_props#connect#activate
    ~callback:(menu_expand_with_options
		 (fun l -> `Across false :: List.filter (function `Across _ -> false | _ -> true) l));
  ctx_menu_expand#connect#activate ~callback:menu_expand;
  ctx_menu_collapse#connect#activate ~callback:menu_collapse;
  ctx_menu_expand_as_tab#connect#activate ~callback:menu_expand_as_tab;
  ctx_menu_collapse_current_tab#connect#activate ~callback:menu_collapse_current_tab;
*)
(*      ctx_menu_join#connect#activate ~callback:(fun () -> menu_join lx); *)
(*      ctx_menu_rename#connect#activate ~callback:(fun () -> if nb_selected = 1 then menu_rename ()); *)
  ctx_menu_open_url#connect#activate
    ~callback:(fun () -> Option.iter open_url selected_iri);
(*
  ctx_menu_define_label#connect#activate
    ~callback:(fun () -> Option.iter (fun uri -> menu_define_label Rdfs.uri_label "en" uri ()) selected_iri);
  ctx_menu_define_inverse_label#connect#activate
    ~callback:(fun () -> Option.iter (fun uri -> menu_define_label Lisql.Lisql.uri_inverseLabel "en" uri ()) selected_iri);
*)
  ctx_menu_define_ns#connect#activate
    ~callback:(fun () -> Option.iter (fun iri -> menu_define_ns iri ()) selected_iri);
  ctx_menu_rename#connect#activate
    ~callback:(fun () ->
      match selected_iri with
      | None -> assert false
      | Some iri ->
	iri_dialog history#store
	  ~title:"Rename an entity"
	  ?fragment:(fragment_of_iri history#store#base iri)
	  ~callback:(fun new_iri ->
	    history#store#rename iri new_iri;
	    refresh true)
	  ());
(*
  ctx_menu_super_class#connect#activate
    ~callback:(fun () -> menu_join Class lx);
  ctx_menu_super_property#connect#activate
    ~callback:(fun () -> menu_join Property lx);
  ctx_menu_super_inverse_property#connect#activate
    ~callback:(fun () -> menu_join InverseProperty lx);
  ctx_menu_import_iris#connect#activate
    ~callback:(fun () -> import_iris selected_iris);
*)
  ()

let menu_declare_query () =
  if history#current#place#is_necessary || history#current#place#has_instance then
    error_message "Entities of this description are already declared."
  else begin
    history#current#place#make_possible;
    refresh true
  end

let menu_enforce_query () =
  handle_axiom_status (history#current#place#make_necessary_at_focus)

let menu_retract_query () =
  handle_axiom_status (history#current#place#make_impossible)

let menu_name_query () =
  iri_dialog history#store
    ~title:"Define an IRI for the named class"
    ~callback:(fun iri ->
      history#current#place#name iri;
      refresh true)
    ()

let menu_instance_query () =
  iri_dialog history#store
    ~title:"Define an IRI for the named individual"
    ~callback:(fun iri ->
      history#current#place#add_instance iri;
      refresh true)
    ()

let menu_anonymous_query () =
  history#current#place#add_anonymous_instance;
  refresh true

let menu_class_adjunct () =
  iri_dialog history#store
    ~title:"Addition of a possible class"
    ~callback:(fun iri ->
      history#store#add_class iri;
      refresh true)
    ()
let menu_property_adjunct () =
  iri_dialog history#store
    ~title:"Addition of a possible property"
    ~callback:(fun iri ->
      history#store#add_property iri;
      refresh true)
    ()

let query_menu_clicked query_menu =
  let query_menu_factory = new GMenu.factory query_menu in
  let cmd_declare = query_menu_factory#add_item "Possible" in
  let cmd_enforce = query_menu_factory#add_item "Necessary" in
  let cmd_retract = query_menu_factory#add_item "Impossible" in
  let cmd_name = query_menu_factory#add_item "Define class" in
  let cmd_instance = query_menu_factory#add_item "Add instance" in
  let _ = query_menu_factory#add_separator () in
  let cmd_union = query_menu_factory#add_item "... or ?" in
  let cmd_compl = query_menu_factory#add_item "not ..." in
  let cmd_select = query_menu_factory#add_item "Select" in
  let cmd_delete = query_menu_factory#add_item "Delete" in
  let cmd_reverse = query_menu_factory#add_item "Reverse" in
  let _ = query_menu_factory#add_separator () in
  let cmd_copy = query_menu_factory#add_item "Copy" in
  let cmd_cut = query_menu_factory#add_item "Cut" in
  let cmd_paste = query_menu_factory#add_item "Paste" in
  cmd_declare#connect#activate ~callback:menu_declare_query;
  cmd_enforce#connect#activate ~callback:menu_enforce_query;
  cmd_retract#connect#activate ~callback:menu_retract_query;
  cmd_name#connect#activate ~callback:menu_name_query;
  cmd_instance#connect#activate ~callback:menu_instance_query;
  cmd_union#connect#activate ~callback:
    (fun () -> query_transf_focus Expr.focus_union);
  cmd_compl#connect#activate ~callback:
    (fun () ->
      if history#current#place#is_necessary_at_focus
      then error_message "The complement is not applicable here. This would make the current description unsatisfiable"
      else query_transf_focus Expr.focus_compl);
  cmd_select#connect#activate ~callback:
    (fun () -> query_transf_focus Expr.focus_select);
  cmd_delete#connect#activate ~callback:
    (fun () -> query_transf_focus Expr.focus_delete);
  cmd_reverse#connect#activate ~callback:
    (fun () -> query_transf_focus (fun foc_q -> [], Expr.focus_reversal foc_q));
  cmd_copy#connect#activate ~callback:(fun () -> wq#copy);
  cmd_cut#connect#activate ~callback:(fun () -> wq#cut);
  cmd_paste#connect#activate ~callback:(fun () -> wq#paste);
  ()

(* Building the GUI *)

let main () =
  window#set_title (utf (title ()));

  (* callbacks *)
  window#event#connect#delete ~callback:(fun _ -> menu_quit (); true) (*fun () -> Context.init ""; GMain.quit ()*);

  window#event#connect#key_press ~callback:(fun key ->
    let keyval = GdkEvent.Key.keyval key in
    let modifs = GdkEvent.Key.state key in
    if keyval = _Tab (* && List.mem `CONTROL modifs && not (List.mem `SHIFT modifs) *)
    then begin menu_cd (Expr.focus_next_prefix history#current#focus_query); true end
    else false);

  cmd_new#connect#activate ~callback:menu_new;
  cmd_open#connect#activate ~callback:menu_open;
  cmd_save#connect#activate ~callback:menu_save;
  cmd_save#add_accelerator ~group:accel_group ~modi:[`CONTROL] _s;
  cmd_saveas#connect#activate ~callback:menu_saveas;
(*
   cmd_close#connect#activate ~callback:menu_close;
 *)
  cmd_stat#connect#activate ~callback:menu_stat;
  cmd_quit#connect#activate ~callback:menu_quit;

(*
  cmd_back#connect#activate ~callback:menu_back;
  cmd_back#add_accelerator ~group:accel_group ~modi:[`CONTROL] _b;
  cmd_fwd#connect#activate ~callback:menu_fwd;
  cmd_fwd#add_accelerator ~group:accel_group ~modi:[`CONTROL] _f;
  cmd_refresh#connect#activate ~callback:menu_refresh;
  cmd_refresh#add_accelerator ~group:accel_group ~modi:[`CONTROL] _r;
*)
  radio_manchester#connect#activate ~callback:(fun () ->
    syntax := Expr.print_manchester;
    refresh true);
  radio_dl#connect#activate ~callback:(fun () ->
    syntax := Expr.print_dl;
    refresh true);
  radio_lis#connect#activate ~callback:(fun () ->
    syntax := Expr.print_lis;
    refresh true);
  check_scale#connect#activate ~callback:(fun () -> refresh false); (* only graphical refresh *)
  check_feat_color#connect#activate ~callback:(fun () -> refresh false); (* only graphical refresh *)
  cmd_expand#connect#activate ~callback:menu_expand;
  cmd_collapse#connect#activate ~callback:menu_collapse;

  cmd_about#connect#activate ~callback:(fun () -> info_message
      ~title:"About Possible World Explorer"
      "Possible World Explorer\n\n\
      A tool for exploring the possible worlds of an ontology,\n\
      and for completing it with negative axioms.\n\
    developped by Sebastien Ferre (ferre@irisa.fr)\n\
    co-designed with Sebastian Rudolph (sebastian.rudolph@aifb.uni-karlsruhe.de).");

  button_back#connect#clicked ~callback:menu_back;
  tooltips#set_tip button_back#coerce ~text:"Go backward in the history of queries";
  button_fwd#connect#clicked ~callback:menu_fwd;
  tooltips#set_tip button_fwd#coerce ~text:"Go forward in the history of queries";
  button_refresh#connect#clicked ~callback:menu_refresh;
  tooltips#set_tip button_refresh#coerce ~text:"Refresh the navigation and extent windows";
  button_root#connect#clicked ~callback:menu_root;
  tooltips#set_tip button_root#coerce ~text:"Go to the root query";
  button_undo#connect#clicked ~callback:menu_undo;
  tooltips#set_tip button_undo#coerce ~text:"Undo last axiom";
  button_save#connect#clicked ~callback:menu_save;
  tooltips#set_tip button_save#coerce ~text:"Save changes to the current ontology";

  button_declare#connect#clicked ~callback:menu_declare_query;
  modify_bg button_declare str_color_satisfiable str_color_satisfiable_prelight;
  tooltips#set_tip button_declare#coerce ~text:"Make the current description satisfiable (declare all used entities)";
  button_enforce#connect#clicked ~callback:menu_enforce_query;
  modify_bg button_enforce str_color_necessary str_color_necessary_prelight;
  tooltips#set_tip button_enforce#coerce ~text:"Make the current focus necessary w.r.t. the rest of the description";
  button_retract#connect#clicked ~callback:menu_retract_query;
  modify_bg button_retract str_color_unsatisfiable str_color_unsatisfiable_prelight;
  tooltips#set_tip button_retract#coerce ~text:"Make the current description unsatisfiable";
  button_name#connect#clicked ~callback:menu_name_query;
  tooltips#set_tip button_name#coerce ~text:"Define an equivalent named class";
  
  button_instance#connect#clicked ~callback:menu_instance_query;
  tooltips#set_tip button_instance#coerce ~text:"Add a named individual as an instance";
  button_anonymous#connect#clicked ~callback:menu_anonymous_query;
  tooltips#set_tip button_anonymous#coerce ~text:"Add an anonymous individual as an instance";

  button_class#connect#clicked ~callback:menu_class_adjunct;
  tooltips#set_tip button_class#coerce ~text:"Add a possible class to adjuncts";
  button_property#connect#clicked ~callback:menu_property_adjunct;
  tooltips#set_tip button_property#coerce ~text:"Add a possible property to adjuncts";
  
  wq#connect
    (object
      method ctx_menu = query_menu_clicked
      method focus = focus_query
      method transf = query_transf_focus
    end);

  let re_var = Str.regexp "^[?]\\([A-Za-z0-9]+\\)$" in
  let re_a = Str.regexp "^[ \t]*an?[ \t]+\\([^?]*[^ \t]\\)[ \t]*$" in
  let re_fwd = Str.regexp "^[ \t]*\\([^?]*[^ \t]\\)[ \t]+:[ \t?]*$" in
  let re_bwd = Str.regexp "^[ \t]*\\([^?]*[^ \t]\\)[ \t]+of[ \t?]*$" in
  let re_text = Str.regexp "^[ \t]*[\"]\\(.*\\)$" in
  let re_funct = Str.regexp "^[ \t]*\\([^(]+\\)(" in
  let re_funct_intransitive = Str.regexp "^[ \t]*\\([^?]*[^? \t]\\)[ \t]+with[ \t?]*$" in
  let re_funct_transitive = Str.regexp "^[ \t]*\\([^?]*[^? \t]\\)[ \t]+[?][ \t]+with[ \t?]*$" in
  let re_funct_infix = Str.regexp "^[ \t]*[?][ \t]+\\([^?]*[^? \t]\\)[ \t]+[?][ \t]*$" in
  let re_funct_infix_arg1 = Str.regexp "^[ \t]*this[ \t]+\\([^?]*[^? \t]\\)[ \t]+[?][ \t]*$" in
  let re_funct_infix_arg2 = Str.regexp "^[ \t]*[?][ \t]+\\([^?]*[^? \t]\\)[ \t]+this[ \t]*$" in
  let re_funct_prefix = Str.regexp "^[ \t]*\\([^?]*[^? \t]\\)[ \t]+[?][ \t]*$" in
  let re_funct_prefix_arg1 = Str.regexp "^[ \t]*\\([^?]*[^? \t]\\)[ \t]+this[ \t]*$" in
  let re_funct_postfix = Str.regexp "^[ \t]*[?][ \t]+\\([^?]*[^? \t]\\)[ \t]*$" in
  let re_funct_postfix_arg1 = Str.regexp "^[ \t]*this[ \t]+\\([^?]*[^? \t]\\)[ \t]*$" in

  let ef_callbacks =
    object (self)
      method cd x =
	try
	  let foc_q = Expr.focus_zoom history#store history#current#focus_query x in
	  let foc_q = Expr.focus_next_postfix foc_q in
	  menu_cd foc_q
	with e ->
	  print_endline ("On navigation link: " ^ Printexc.to_string e)

      method cd_input ~mode ~custom =
	let foc_q = history#current#focus_query in
	try
	  if custom = "or" then menu_cd (Expr.focus_union foc_q)
	  else if custom = "not" then menu_cd (Expr.focus_compl foc_q)
	  else if custom = "select" || custom = "sel" then menu_cd (Expr.focus_select foc_q)
	  else if custom = "delete" || custom = "del" then menu_cd (Expr.focus_delete foc_q)
	  else (*failwith ""*)
(* TODO *)
	      begin
(*
		try
		  let x = Class.Name (literal_of_string custom) in
		  self#cd x
		with _ ->
*)
		let kind =
(*
		  if Str.string_match re_var custom 0 then `Var (Str.matched_group 1 custom)
		  else
*)
		  if Str.string_match re_a custom 0 then `A (Str.matched_group 1 custom)
		  else if Str.string_match re_fwd custom 0 then `Fwd (Str.matched_group 1 custom)
		  else if Str.string_match re_bwd custom 0 then `Bwd (Str.matched_group 1 custom)
(*
		  else if Str.string_match re_text custom 0 then `Text (Str.matched_group 1 custom)
		  else if Str.string_match re_funct custom 0 then `Funct (Str.matched_group 1 custom, 1, 0, Term.uri_Functor)
		  else if Str.string_match re_funct_intransitive custom 0 then `Funct (Str.matched_group 1 custom, 1, 1, Term.uri_IntransitiveVerb)
		  else if Str.string_match re_funct_transitive custom 0 then `Funct (Str.matched_group 1 custom, 2, 1, Term.uri_TransitiveVerb)
		  else if Str.string_match re_funct_infix custom 0 then `Funct (Str.matched_group 1 custom, 2, 0, Term.uri_InfixOperator)
		  else if Str.string_match re_funct_infix_arg1 custom 0 then `Funct (Str.matched_group 1 custom, 2, 1, Term.uri_InfixOperator)
		  else if Str.string_match re_funct_infix_arg2 custom 0 then `Funct (Str.matched_group 1 custom, 2, 2, Term.uri_InfixOperator)
		  else if Str.string_match re_funct_prefix custom 0 then `Funct (Str.matched_group 1 custom, 1, 0, Term.uri_PrefixOperator)
		  else if Str.string_match re_funct_prefix_arg1 custom 0 then `Funct (Str.matched_group 1 custom, 1, 1, Term.uri_PrefixOperator)
		  else if Str.string_match re_funct_postfix custom 0 then `Funct (Str.matched_group 1 custom, 1, 0, Term.uri_PostfixOperator)
		  else if Str.string_match re_funct_postfix_arg1 custom 0 then `Funct (Str.matched_group 1 custom, 1, 1, Term.uri_PostfixOperator)
*)
		  else `Other (custom) in
	      match kind with
(*
	      | `Var name ->
		  let x = Class.Var name in
		  Logui.item Logui.uri_VariableEntry (fun obs ->
		    [ (Logui.uri_class, [Turtle.Typed (string_of_concept x, Logui.lisql_Class)]) ]);
		  self#cd x
*)
	      | `A name ->
		  iri_dialog history#store
		    ~title:"Creation of a class"
		    ~fragment:name
		    ~callback:(fun iri ->
		      let x = Expr.Class iri in
		      self#cd x)
		    ()
	      | `Fwd name ->
		  iri_dialog history#store
		    ~title:"Creation of an object property restriction"
		    ~fragment:name
		    ~callback:(fun iri ->
		      let x = Expr.ObjectExists (Expr.ObjectProperty iri, Expr.Thing) in
		      self#cd x)
		    ()
	      | `Bwd name ->
		  iri_dialog history#store
		    ~title:"Creation of an inverse object property restriction"
		    ~fragment:name
		    ~callback:(fun iri ->
		      let x = Expr.ObjectExists (Expr.Inverse (Expr.ObjectProperty iri), Expr.Thing) in
		      self#cd x)
		    ()
(*
	      | `Text text ->
		  ( match GToolbox.input_text ~title:"Creation of a text" ~text:text "Enter a text" with
		  | None -> ()
		  | Some text ->
		      let x = Class.Name (Rdf.Literal (text, Rdf.Plain "")) in
		      Logui.item Logui.uri_TextCreation (fun obs ->
			[ (Logui.uri_class, [Turtle.Typed (string_of_concept x, Logui.lisql_Class)]) ]);
		      self#cd x )
	      | `Funct (name, arity, focus, functor_type) ->
		  functor_dialog history#store
		    ~title:"Creation of a functor"
		    ~label:name
		    ~arity:arity
		    ~focus:focus
		    ~functor_type:functor_type
		    ~callback:(fun (uri_funct,args_arity,focus) ->
		      let x = make_struct_arg uri_funct args_arity focus in
		      Logui.item Logui.uri_StructureEntry (fun obs ->
			[ (Logui.uri_class, [Turtle.Typed (string_of_concept x, Logui.lisql_Class)]) ]);
		      self#cd x)
		    ()
*)
	      | `Other text ->
		  iri_dialog history#store
		    ~title:"Creation of an individual"
		    ~fragment:text
		    ~callback:(fun iri ->
		      let x = Expr.Individual iri in
		      self#cd x)
		    ()
	      end;
	  true
	with exn ->
	  error_message ("This is not a valid navigation link: " (* ^ Printexc.to_string exn *));
	  false

    end in
  focus_ef#connect ef_callbacks;
  ef#connect ef_callbacks;

  navig#connect
    (object
      method row_activated mode iter =
	let x = navig#class_from_iter iter in
	match mode with
	| `Select ->
	  let foc_q = Expr.focus_zoom history#store history#current#focus_query x in
	  let foc_q = Expr.focus_next_postfix foc_q in
	  menu_cd foc_q
	| `AddChild ->
	  ( match x with
	  | Expr.Class iri -> menu_add_subclass iri
	  | Expr.ObjectExists (Expr.ObjectProperty iri, _) -> menu_add_subproperty iri
	  | Expr.ObjectExists (Expr.Inverse (Expr.ObjectProperty iri), _) -> menu_add_subproperty iri
	  | _ -> () )
	| `Necessary ->
	  handle_axiom_status (history#current#place#make_all_necessary [x])
	| `Impossible ->
	  handle_axiom_status (history#current#place#make_all_impossible [x])

      method row_expanded path = ()

      method row_collapsed path = ()

      method ctx_menu = ctx_menu_clicked

      method drag_and_drop lx x = menu_all_subsumedby lx x
    end);

  window#set_default_size ~width:1000 ~height:800;
  window#add_accel_group accel_group;
  window#show ();
  paned_set_ratio paned_global 2 5;
(*      paned_set_ratio paned_sw 1 2; *)
  navig#set_ratio_extent 1 2;
  navig#set_ratio_facets 1 2;
  awaken (fun () -> ()) refresh_force;
  GMain.main ()

let _ =
  Owlapi.debug main
